<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/post','PostController@index');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    Route::get('/', ['uses' => 'PostController@dashboard', 'as' => 'voyager.dashboard'])->middleware('isAuth');
    Route::get('themes/edit/{id}', ['uses' => 'ThemeController@edit', 'as' => 'edit-theme'])->middleware('isAuth');
    Route::get('themes/delete/{id}', ['uses' => 'ThemeController@delete', 'as' => 'delete-theme'])->middleware('isAuth');
    Route::get('themes/create', ['uses' => 'ThemeController@create', 'as' => 'create-theme'])->middleware('isAuth');
    Route::post('themes/store', ['uses' => 'ThemeController@store', 'as' => 'store-theme'])->middleware('isAuth');
    Route::post('themes/update', ['uses' => 'ThemeController@update', 'as' => 'update-theme'])->middleware('isAuth');

    Route::get('posts/list/{theme_id}', ['uses' => 'PostController@list', 'as' => 'list-posts'])->middleware('isAuth');
    Route::get('posts/list-per-retailer/{client_id}', ['uses' => 'PostController@listPerClient', 'as' => 'list-posts-per-retailer'])->middleware('isAuth');
    Route::get('posts/edit/{id}', ['uses' => 'PostController@edit', 'as' => 'edit-post'])->middleware('isAuth');
    Route::post('posts/update', ['uses' => 'PostController@update', 'as' => 'update-post'])->middleware('isAuth');

    Route::get('likes/list/{post_id}', ['uses' => 'LikeController@list', 'as' => 'list-like'])->middleware('isAuth');

    Route::get('clients/list/{client_id}', ['uses' => 'RetailerController@list', 'as' => 'client-info'])->middleware('isAuth');
    /*new routes*/
    Route::post('change-date', ['uses' => 'ReportingController@getDates', 'as' => 'change-date'])->middleware('isAuth');

    Route::get('reportings', ['uses' => 'ReportingController@index', 'as' => 'reporting-index'])->middleware('isAuth');

    Route::post('get-top-post', ['uses' => 'ReportingController@top_post', 'as' => 'get-top-post'])->middleware('isAuth');

    Route::get('shared-post/{id}', ['uses' => 'ReportingController@sharedPost', 'as' => 'shared-post'])->middleware('isAuth');

    Route::post('filter-chart', ['uses' => 'ReportingController@filterChart', 'as' => 'filter-chart']);
    /*new routes for comments*/
    Route::get('edit-comment/{id}', ['uses' => 'CommentController@edit', 'as' => 'edit-comment'])->middleware('isAuth');
    Route::get('list-comment/{id}', ['uses' => 'CommentController@list', 'as' => 'list-comment'])->middleware('isAuth');
    Route::get('retailer-comment-retailer/{id}', ['uses' => 'CommentController@retailerComment', 'as' => 'retailer-comment'])->middleware('isAuth');
    Route::post('validation-comment', ['uses' => 'commentController@validation', 'as' => 'validation-comment'])->middleware('isAuth');
    Route::get('likes-comment/{id}', ['uses' => 'CommentController@LikesComment', 'as' => 'likes-comment'])->middleware('isAuth');
    /*new route for retailer filtre*/
    Route::get('filter-retailer/{model}/{type}/{id}', ['uses' => 'RetailerController@retailerFilter', 'as' => 'filter-retailer'])->middleware('isAuth');
    /*new route delete comment post*/
    Route::get('delete-comment-post/{type}/{id}', ['uses' => 'CommentController@deleteCommentPost', 'as' => 'delete-cp'])->middleware('isAuth');
});
