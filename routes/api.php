<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('get-all-themes', 'ThemeController@getAllThemes')->middleware('checkToken');
Route::post('store-post', 'PostController@storePost')->middleware('checkToken');
Route::post('store-like', 'PostController@storeLike')->middleware('checkToken');
Route::post('posts-list', 'PostController@postList')->middleware('checkToken');
Route::post('likes-list', 'PostController@listLike')->middleware('checkToken');
Route::post('post-likes', 'PostController@postLikes')->middleware('checkToken');

//New for Reporting
Route::post('store-duration', 'DurationController@storeDuration')->middleware('checkToken');
Route::post('client-posts', 'PostController@retailerPosts')->middleware('checkToken');
Route::post('delete-post', 'PostController@deletePost')->middleware('checkToken');
 // New for Comment
Route::post('my-comments', 'CommentController@myComments')->middleware('checkToken');
Route::post('delete-comment', 'CommentController@deleteComment')->middleware('checkToken');
Route::post('edit-comment', 'CommentController@editComment')->middleware('checkToken');
Route::post('update-comment', 'CommentController@updateComment')->middleware('checkToken');
Route::post('store-comment', 'CommentController@storeComment')->middleware('checkToken');
Route::post('list-comment', 'CommentController@listComments')->middleware('checkToken');
Route::post('store-comment-likes', 'CommentController@storeLike')->middleware('checkToken');
Route::post('comment-likes', 'CommentController@listLikes')->middleware('checkToken');
