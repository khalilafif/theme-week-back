<?php

namespace App\Http\Middleware;

use Closure;

class CheckToken
{   
    public $retailer;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token=$request->token;
        //$token="LDDFnDrHsxDhH5Cfz6Iu4g0LjfFbuu8AEvf/4fXzqXAaov3WL9fukTYfM7Y+jQ435reBg/xKtqdFA4qKFEy7feq/4aj1aG6Jn8wF6a7zZuZXVwQADp9VtTJaPr64ivrGXmwY5zdh1pA8isWIGA3GRZy51wrrxsJ/bUSnJwUThDhtVkdKjpYDT4JNOF6lXgFEw0LAEnjz/MV12Y6vkPxYrCmkKhih3i0AExB69FV3xhh6g282zRNLEutoOfzQQ++9x+WrMVg4KhLI1QurESSD5Qdswd4PZIDFLpwQKyzG5wwP6eU7i3yG33xSE/chzwOrezAA0QAbIXjmoN8zh7JGXygG4LXifF0u8w10QrQ9Ggy933bi2fVf9ft5gVoKvVa7oMtbtkv5nM3QZCH3RxuV9DTuUDy8IrV4xG2+UBrL60xRn9dg3WuExqwD11+bRAP5uytplBMlhzZ2cgIHz13CJLj53U0KahUierMIBwzbdKNy/5tYVOmNn0N3qGUe83aREtr+BMMhMnDLmDJptUYrTiAYzFwhwjauTYBCnfkZUJkpxuR16If74LDMxwf/VwRFC+fraefILucMvKKu05K4hx8/rRHb5EJm7kkkYGSHGwfNj8JZcMNcrEy0HGB5gyvxIGk8c4ZmwC5QBN9g00DmV5hFViePFYbIBcqtPuFDsK0=";
        if(!$this->getCustomerFromToken($token)){
            return response(['status'=>'error','message'=>'token invalid'], 200);
        }
        $request->attributes->add(['retailer' => $this->retailer]);
        return $next($request);
    }
    public function getCustomerFromToken($token){
		$token=str_replace(" ","+",$token);
        $user_converted=$this->decryptQuery($token);
        if(count($user_converted)<=1 && !isset($user_converted['UserId'])) return false;
        $this->retailer=$user_converted;		
		return true;
    }
    
    public function decryptQuery($query){
        $pieces = explode("&", $this->my_decrypt($query, 'AC37A27EF8391CD3AC37A27EF8391CD3'));
   		$result=array();
   		foreach ($pieces as $piece) {
   			$pos=strpos($piece, "=");
   			$key=substr($piece, 0, $pos);
   			$value=substr($piece, $pos+1, strlen($piece)-$pos+1);
   			$result[$key]=$value;   	
   		}
    	return $result;
    }
    
    public function my_decrypt($data, $key) {
		$encryption_key = openssl_random_pseudo_bytes(32);
		$iv =  'TestPMI 12345678';	
        $decrypted = openssl_decrypt ( base64_decode($data) , "AES-256-CBC" , $key , OPENSSL_RAW_DATA, $iv);
		return $decrypted;   
	}

}
