<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Request as RQ;
use App\Post;
use App\Theme;
use App\Client;
use App\Like;
use App\Likec;
use DB;
class PostController extends Controller
{   
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
        $posts = Post::where('is_deleted',0)->with('theme')->with('user')->with('client')->withCount(['likes'=>function($query){
            $query->where('dislike',0);
        }])->
        withCount(['comments'=>function($query){
            $query->where('is_deleted',0)
                  ->where('state','acceptee');
        }])->orderByDesc('created_at')->get();
        $themes=Theme::all();
        $topPost=$this->getTopPost($themes->first()->id); 
        return view('vendor/voyager/posts/browse',compact('posts','themes','topPost')); 
    }
    
    public function dashboard(){//en attente, acceptee, refusee
        $posts = Post::where('state','acceptee')->
                        where('is_deleted',0)->
                        orderByDesc('created_at')->
                        get(); 
        $comments=DB::table('comments')->
                    where('state','acceptee')->
                    where('is_deleted',0)->
                    select(DB::raw('post_id as post'),
                           DB::raw('count(*) as total'))->
                    groupBy('post')->
                    get()->pluck('total','post');
        $likes=DB::table('likes')->
                    where('dislike',0)->
                    select(DB::raw('post_id as post'),
                           DB::raw('count(*) as total'))->
                    groupBy('post')->
                    get()->pluck('total','post');        
        $clients=DB::table('clients')->
                    select('firstname','lastname',DB::raw('id as id'))->
                    get()->
                    groupBy('id');
        $themes=Theme::all()->pluck('title','id');
        return view('dashboard',compact('posts','comments','likes','clients','themes')); 
    }

    public function getCustomer($user){
		$client=null;
		if(isset($user["UserId"]) && is_null($client=Client::where('userid',$user["UserId"])->first())){
			$client= new Client();
			$client->userid=(isset($user["UserId"])) ? $user["UserId"] : null ;
			$client->contactid=(isset($user["ContactId"])) ? $user["ContactId"] : null ;
			$client->firstname=(isset($user["FirstName"])) ? $user["FirstName"] : null ;
			$client->lastname=(isset($user["LastName"])) ? $user["LastName"] : null ;
			$client->role=(isset($user["Role"])) ? $user["Role"] : null ;
			$client->email=(isset($user["Email"])) ? $user["Email"] : null ;
			$client->language=(isset($user["Language"])) ? $user["Language"] : null ;
			$client->sessionid=(isset($user["SessionId"])) ? $user["SessionId"] : null ;
			$client->touchpointids=(isset($user["TouchpointIDs"]));
			$client->tradeprogram=(isset($user["TouchpointIDs"])) ? $user["TouchpointIDs"] : null ;
			$client->reasontype=(isset($user["ReasonType"])) ? $user["ReasonType"] : null ;
			$client->awardpoints=(isset($user["AwardPoints"])) ? $user["AwardPoints"] : null ;
			$client->reasondescription=(isset($user["ReasonDescription"])) ? $user["ReasonDescription"] : null ;
			$client->mailingcity=(isset($user["MailingCity"])) ? $user["MailingCity"] : null ;
			$client->mobilephone=(isset($user["MobilePhone"])) ? $user["MobilePhone"] : null ;
			$client->origin=(isset($user["Origin"])) ? $user["Origin"] : null ;
			$client->save();
		}else{
			$client->sessionid=(isset($user["SessionId"])) ? $user["SessionId"] : null ;
            $client->awardpoints=(isset($user["AwardPoints"])) ? $user["AwardPoints"] : null ;
            $client->update();
		}
		return $client;
	}

    public function getTopPost($theme_id){
        $post=Post::where('state','acceptee')->
            where('is_deleted',0)->where('theme_id',$theme_id)->withCount('likes')->orderBy('likes_count', 'desc')->first();
        return $post;
    }
    public function getTopPostByClient($client_id){
        $post=Post::where('state','acceptee')->
            where('is_deleted',0)->where('client_id',$client_id)->withCount('likes')->orderBy('likes_count', 'desc')->first();
        return $post;
    }
 
    /*** for API ***/
    public function storePost(Request $request)
    {
        if(isset($request->title) && isset($request->description) && isset($request->theme_id) ){
            $client = $this->getCustomer(\Request::get('retailer'));
            $post=new Post();
            $post->state="en attente";
            $post->title=$request->title;
            $post->description=$request->description;
            if($request->hasFile('image')) $post->media=$this->uploadMedia($request,'image',$client->id);
            $post->share=0;
            $post->theme_id=$request->theme_id;
            $post->client_id=$client->id;
            $post->save();
            return response(['status'=>'success','message'=>'post saved','data'=>'saved'], 200); 
        }else{
            return response(['status'=>'success',"data"=>'not saved','message'=>'data not valid'], 200);
        }        
    }
    /*** for API ***/
    public function storeLike(Request $request)
    {
        if(isset($request->post_id)){            
            $client = $this->getCustomer(\Request::get('retailer'));
            $oldLike=Like::where('client_id',$client->id)->where('post_id',$request->post_id)->first();
            if(empty($oldLike)){                
                $like=new Like();
                $like->client_id=$client->id;
                $like->post_id=$request->post_id;
                $like->dislike=0;
                $like->save();
                return response(['status'=>'success','message'=>'like saved','data'=>['dislike'=>0]], 200);
            }else{
                if($oldLike->dislike){
                    $oldLike->dislike=0;
                    $oldLike->update();
                    return response(['status'=>'success','message'=>'like','data'=>['dislike'=>0]], 200);
                }else{
                    $oldLike->dislike=1;
                    $oldLike->update();
                    return response(['status'=>'success','message'=>'dislike','data'=>['dislike'=>1]], 200);
                } 
            }
        }else{
            return response(['status'=>'success','message'=>'data not valid','data'=>false], 200);
        }
        
    }
     /*** for API ***/
     public function listLike(Request $request)
     {         
            $client = $this->getCustomer(\Request::get('retailer'));
            $posts=$this->getLikesList('posts','Like','post_id',$client->id);
            $comments=$this->getLikesList('comments','Likec','comment_id',$client->id);
            $likes= $posts->merge($comments)->sortByDesc('created_at');

            return response(['status'=>'success','message'=>'like list','data'=>$likes->values()->all()], 200);          
         
     }
     public function getLikesList($table,$likeTable,$column,$client_id){
        $likes=collect([]);
        $data=DB::table($table)->select('id')->where('client_id',$client_id)->where('is_deleted',0)->where('state','acceptee')->get();
        $idsData=$data->pluck('id')->toArray();
        if(!empty($idsData)){
            if($table=='comments')
                $likes= Likec::where('dislike',0);
            else
                $likes= Like::where('dislike',0);
            $likes=$likes->whereIn($column, $idsData)->
                           with('client')->
                           orderByDesc('created_at')->
                           get();
        }
        return $likes;
     }
     /*** for API ***/
     public function postLikes(Request $request)
     {         
        if(isset($request->post_id)){            
            $likes= Like::where('dislike',0)->where('post_id',$request->post_id)->with('client')->orderByDesc('created_at')->get();
            return response(['status'=>'success','message'=>'post like list','data'=>$likes], 200);
        }else{
            return response(['status'=>'success','message'=>'data not valid','data'=>false], 200);
        }  
     }

     /*** for API ***/
     public function retailerPosts(Request $request)
     {         
        //if(isset($request->client_id)){            
            $client = $this->getCustomer(\Request::get('retailer'));
            $posts= Post::where('client_id',$client->id)->
            where('is_deleted',0)->orderByDesc('created_at')->get();
            return response(['status'=>'success','message'=>'post list','data'=>$posts], 200);
       /* }else{
            return response(['status'=>'success','message'=>'data not valid','data'=>false], 200);
        }*/  
     }

    public function uploadMedia(Request $request,$input_name,$client_id){
            $file = $request->file($input_name);
            $path = base_path().'/public/uploaded';
            $filename=$client_id.'_'.strtotime(date("Y-m-d h:i:sa")).'.'.$file->getClientOriginalExtension();
            $file->move($path,$filename);            
            return \URL::to('/').'/uploaded/'.$filename;
    }
  

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post=Post::findOrFail($id);
    	return view('vendor/voyager/posts/edit-add',compact('post')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request,[ 
            'state'=>'required',
            'post_id'=>'required']);

        $post=Post::findOrFail($request->post_id);

        $post->state=$request->state;
        if($request->state=="refusee"){ $post->reason=$request->reason;}
        else{ $post->reason=null;}
        $post->user_id=auth()->user()->id;
        $post->update();
        return redirect('admin/posts');

    } 

    public function list($theme_id){
        $posts = Post::where('theme_id',$theme_id)->
            where('is_deleted',0)->with('theme')->with('user')->with('client')->with(['likes'=>function($query){
            $query->where('dislike',0);
        }])->withCount(['comments'=>function($query){
            $query->where('is_deleted',0)
                  ->where('state','acceptee');
        }])->orderByDesc('created_at')->get(); 
        $themes=Theme::all();
        $topPost=$this->getTopPost($theme_id); 
        $withTheme=true;
        return view('vendor/voyager/posts/browse',compact('posts','themes','topPost','withTheme')); 
    }
    public function listPerClient($client_id){       
        $posts = Post::where('client_id',$client_id)->
                       where('state','!=','en attente')->
                       //where('is_deleted',0)->
                       with('theme')->with('user')->
                       with('client')->
                       withCount(['likes'=>function($query){
                            $query->where('dislike',0);
                        }])->withCount(['comments'=>function($query){
                            $query->where('is_deleted',0)->where('state','acceptee');
                        }])->orderByDesc('created_at')->get(); 
        $themes=Theme::all();
        $topPost=$this->getTopPost($themes->first()->id); 
        return view('vendor/voyager/posts/browse',compact('posts','themes','topPost')); 
    }
    public function postList(Request $request){
        if(isset($request->theme_id)){
            $posts=Post::
                where('theme_id',$request->theme_id)->
                where('is_deleted',0)->
                where('state','acceptee')->
                with(['likes'=>function($query){
                    $query->where('dislike',0);}])->
                with(['comments'=>function($query){
                    $query->where('is_deleted',0)->
                            where('state','acceptee')->
                            with('client')->
                            with(['likecs'=>function($query1){
                                $query1->where('dislike',0);}]);}])->
                with('client')->orderByDesc('created_at')->get();
            return response(['status'=>'success','message'=>'posts list','data'=>$posts], 200); 
        } 
        return response(['status'=>'success','message'=>'data not valid','data'=>false], 200);
    }
    /*** for API ***/
    public function deletePost(Request $request)
    {
        if(isset($request->post_id)){
            $client = $this->getCustomer(\Request::get('retailer'));
            $post=Post::where('id',$request->post_id)->where('client_id',$client->id)->update(['is_deleted'=>1]);
            if($post){
                return response(['status'=>'success','message'=>'post deleted','data'=>true], 200); 
            }else{
                return response(['status'=>'success','message'=>'post error','data'=>false], 200);
            }
        }else{
            return response(['status'=>'success',"data"=>'not saved','message'=>'data not valid'], 200);
        }        
    }
    
}
