<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Like;
class LikeController extends Controller
{
    public function index(){
        $likes=Like::where('dislike',0)->with('client')->get();
        return view('vendor/voyager/likes/list',compact('likes')); 
    }
    public function list($post_id){
        $likes=Like::where('dislike',0)->where('post_id',$post_id)->with('client')->get();
        return view('vendor/voyager/likes/list',compact('likes')); 
    }
    
}
