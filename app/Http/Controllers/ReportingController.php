<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Duration;
use App\Like;
use App\Theme;
use App\Post;
use DB;
class ReportingController extends Controller
{
    public function index(){
        $themes=Theme::all();
        $date_debut=$themes->first()->date_begin;
        $date_fin=$themes->last()->date_end;

        return $this->report($date_debut,$date_fin,null,true);
    }
    public function getDatePeriode($date_debut,$date_fin){
        $result=array();
        $date_fin=date('Y-m-d', strtotime("+1 days", strtotime(date($date_fin))));
        $period = new \DatePeriod(new \DateTime($date_debut),
                                  new \DateInterval('P1D'),
                                  new \DateTime($date_fin));
        foreach ($period as $key => $value) {
            $result[$value->format('Y-m-d')]=0;
        }
        return $result;
    }

    public function getDateTimePeriode($date_debut,$date_fin){
        $begin = new \DateTime($date_debut.' 00:00');
        $end = new \DateTime( $date_fin.' 24:00');
        //$end = $end->modify( '+1 day' );
        $result=array();
        $interval = new \DateInterval('PT3600S');
        $daterange = new \DatePeriod($begin, $interval ,$end);

        foreach($daterange as $date){
            $result[ $date->format("Y-m-d H:00")]=0;
        }
        return $result;
    }

    public function getDates(Request $request){
        /*$this->validate($request,[
            'datedebut'=>'required|date',            'datefin'=>'required|date']);*/
           //dd(date('W-F',strtotime("2020-04-02 17:05:19")));
            //dd(date('Y-m-d H:00',strtotime("2020-04-02 17:05:19")));
        if((strtotime($request->datedebut) > strtotime($request->datefin)) || is_null($request->datedebut) || is_null($request->datefin)){
                        return redirect(route('reporting-index'))->withErrors('Date invalide');
                }
        if(!$request->theme){
            $theme=null;
            $global=true;
        }else{
            $theme=Theme::findOrFail($request->theme);
            $global=false;
        }
                return $this->report($request->datedebut,$request->datefin,$theme,$global);
    }

    public function report($date_debut,$date_fin,$sTheme,$global_stat){
        $metrics=array();
        $period=$this->getDatePeriode($date_debut,$date_fin);
        $clients=Client::where('created_at', '<=',$date_fin." 23:59:59")->get();
        $newClients=$clients->whereBetween('created_at', [$date_debut, $date_fin." 23:59:59"]);
        $posts=Post::whereBetween('created_at', [$date_debut, $date_fin])->with('theme')->with('client')->with(['likes'=>function($query){
            $query->where('dislike',0);
        }])->get();
        $durations=Duration::whereBetween('created_at', [$date_debut, $date_fin." 23:59:59"])->get();
        $likes=Like::whereBetween('created_at', [$date_debut, $date_fin." 23:59:59"])->get();
        $themes=Theme::with('posts')->get();
        $nbPostByTheme=$this->getNbPostByTheme($themes);
        $countPostByDate=$this->getCountByCreatedAt('posts',$date_debut, $date_fin." 23:59:59",$period,'created_at');
        $countCommentByDate=$this->getCountByCreatedAt('comments',$date_debut, $date_fin." 23:59:59",$period,'created_at');
        $countLikecByDate=$this->getCountByCreatedAt('likecs',$date_debut, $date_fin." 23:59:59",$period,'updated_at');
        $countUnicCilent=$this->getChartUnicUser($date_debut, $date_fin." 23:59:59",$period);
        $countActiveClientByDate=$this->getChartActiveUser($date_debut, $date_fin." 23:59:59",$period);
       // dd($countActiveClientByDate,$period,$date_debut, $date_fin);
        $countNewCilent=$this->getCountByCreatedAt('clients',$date_debut, $date_fin." 23:59:59",$period,'created_at');
        $countLikeByDate=$this->getCountByCreatedAt('likes',$date_debut, $date_fin." 23:59:59",$period,'updated_at');
        $countDurationByDate=$this->getDurationByTheme($date_debut,$date_fin." 23:59:59",$period);
        $avgSessionDuration=$this->getAvgDuration($date_debut,$date_fin/*." 23:59:59"*/);
        //dd();
        $chartSessionByDate=$this->getCountSession($date_debut, $date_fin." 23:59:59",$period);
        if(is_null($sTheme)){
           // $sTheme=$themes->first();
            $sThemeId=null;
        }else{
            $sThemeId=$sTheme->id;
        }
        $topPost=$this->getTopPost($sThemeId,$date_debut,$date_fin);
        //dd($topPost);
        $regions=$this->getGountByRegion($date_debut, $date_fin." 23:59:59");
        $countUnicUser=$this->getCountUnicUser($date_debut, $date_fin." 23:59:59");

        return view('reporting',compact('date_debut','date_fin','metrics','global_stat','topPost','regions','countActiveClientByDate','countPostByDate','themes','sTheme','countLikeByDate','countDurationByDate','posts','period','countUnicCilent','countNewCilent','chartSessionByDate','nbPostByTheme','countUnicUser','avgSessionDuration','countCommentByDate','countLikecByDate'));
    }
    public function getUserActive($date_debut,$date_fin){
        $users = DB::select('SELECT count(*) as total from clients where id
             IN (SELECT client_id FROM posts WHERE created_at between "'.$date_debut.'" AND "'.$date_fin.'")
             OR
             id IN (SELECT client_id FROM likes WHERE created_at between "'.$date_debut.'" AND "'.$date_fin.'")');
        //dd($users);
        return $users[0]->total;
    }
    public function getTopPost($theme_id,$date_begin,$date_end){
        $post=Post::where('state','acceptee');
        if($theme_id)$post=$post->where('theme_id',$theme_id);
        if($date_begin)$post=$post->where('created_at','>=',$date_begin);
        if($date_end)$post=$post->where('created_at','<=',$date_end." 23:59:59");
        return $post->withCount(['likes'=>function($query)use($date_begin,$date_end){
                $query->where('created_at','>=',$date_begin)
                        ->where('created_at','<=',$date_end." 23:59:59")
                        ->where('dislike',0);
        }])/*->withCount('likes')*/->orderBy('likes_count', 'desc')->with('theme')->first();
    }
    public function top_post(Request $request){
        $theme_id=null;
        $date_begin=null;
        $date_end=null;
        if($request->theme_id)$theme_id=$request->theme_id;
        if($request->date_begin)$date_begin=$request->date_begin;
        if($request->date_end)$date_end=$request->date_end;
        return $this->getTopPost($theme_id,$date_begin,$date_end);
    }
    public function getCountByCreatedAt($table,$date_debut, $date_fin,$period,$column){
        $stat=array();
        $stat['total']=0;
        $stat['chart']=array();
        $result=DB::table($table)
            ->whereBetween($column, [$date_debut, $date_fin]);
        if($table=='posts' || $table=='comments')
            $result=$result->where('is_deleted',0)->where('state','acceptee');
        if($table=='likes'|| $table=='likecs')
            $result=$result->where('dislike',0);
       /* if($table=='likes')
            $result=$result->whereHas('post', function($q) {
                $q->where('is_deleted',0)->where('state','acceptee');
            });
        if($table=='likecs')
            $result=$result->whereHas('comment', function($q) {
                $q->where('is_deleted',0)->where('state','acceptee');
            });*/

        $result=$result->select(DB::raw('DATE('.$column.') as date'), DB::raw('count(*) as total'))
            ->groupBy('date')
            ->get();
        $stat['total']=$result->sum('total');
        $resultArr= $result->pluck('total','date')->toArray();
        if(count($period) != count($resultArr)){
            foreach ($resultArr as $key => $value) {
                if(isset($period[$key]))
                  $period[$key]=$value;
            }
            $stat['chart']= array_values($period);
        }else{
            $stat['chart']=array_values($resultArr);
        }
        return $stat;
    }
    function getCountUnicUser($date_debut, $date_fin){
        $result=DB::table('clients')
            ->select(DB::raw('count(*) as total'))
            ->where('created_at','<=',$date_fin)
            ->orwhereBetween('updated_at', [$date_debut, $date_fin])
            ->get();
        return $result->first()->total;
    }
    public function getCountSession($date_debut, $date_fin,$period){
        $stat=array();
        $stat['total']=0;
        $stat['chart']=array();
        $result=DB::table('durations')
            ->whereBetween('created_at', [$date_debut, $date_fin])
            ->select(DB::raw('DATE(created_at) as datec'), DB::raw('count(*) as total'))
            ->groupBy('datec')
            ->get();
        $stat['total']=$result->sum('total');
        $resultArr= $result->pluck('total','datec')->toArray();
        if(count($period) != count($resultArr)){
            foreach ($resultArr as $key => $value) {
                if(isset($period[$key]))
                  $period[$key]=$value;
            }
            $stat['chart']= array_values($period);
        }else{
            $stat['chart']=array_values($resultArr);
        }
        return $stat;
    }
    public function getChartActiveUser($date_debut, $date_fin,$period){
        $stat=array();
        $stat['total']=0;
        $stat['chart']=array();
        /*$result = DB::select('SELECT count(*) as total ,DATE(created_at) as date from clients where
            created_at between "'.$date_debut.'" AND "'.$date_fin.'"
            and
            id
             IN (SELECT client_id FROM posts WHERE created_at between "'.$date_debut.'" AND "'.$date_fin.'")
             OR
             id IN (SELECT client_id FROM likes WHERE created_at between "'.$date_debut.'" AND "'.$date_fin.'") GROUP BY date');*/
        $result = DB::select("select count(distinct client_id) as total, S.date from
                (
                    (select L.client_id, date_format(L.created_at, '%Y-%m-%d') as date from likes L group by client_id, date_format(L.created_at, '%Y-%m-%d'))
                    union
                    (select P.client_id, date_format(P.created_at, '%Y-%m-%d') as date from posts P group by client_id, date_format(P.created_at, '%Y-%m-%d'))
                )
                as S where S.date >= '".$date_debut."' AND S.date <= '".$date_fin."' group by S.date");

        $result=collect($result);
        $stat['total']=$result->sum('total');
        $resultArr= $result->pluck('total','date')->toArray();
        //dd($period);
        if(count($period) != count($resultArr)){
            foreach ($resultArr as $key => $value) {
                if(isset($period[$key]))
                  $period[$key]=$value;
            }
            $stat['chart']= array_values($period);
        }else{
            $stat['chart']=array_values($resultArr);
        }
        return $stat;
    }
    public function getChartUnicUser($date_debut, $date_fin,$period){
        $stat=array();
        $stat['total']=0;
        $stat['chart']=array();
        $result = DB::select("select count(distinct client_id) as total, S.date from
                
                    (select L.client_id, date_format(L.created_at, '%Y-%m-%d') as date from durations L group by client_id, date_format(L.created_at, '%Y-%m-%d'))
                   
                as S where S.date >= '".$date_debut."' AND S.date <= '".$date_fin."' group by S.date");

        $result=collect($result);
        $stat['total']=$result->sum('total');
        $resultArr= $result->pluck('total','date')->toArray();
        //dd($period);
        if(count($period) != count($resultArr)){
            foreach ($resultArr as $key => $value) {
                if(isset($period[$key]))
                  $period[$key]=$value;
            }
            $stat['chart']= array_values($period);
        }else{
            $stat['chart']=array_values($resultArr);
        }
        return $stat;
    }
    public function getGountByRegion($date_debut, $date_fin){
        $result=DB::table('clients')
            ->whereBetween('created_at', [$date_debut, $date_fin])
            ->select(DB::raw('mailingcity as name'), DB::raw('count(*) as y'))
            ->groupBy('name')
            ->get();
        return $result;
    }
    public function getAvgDuration($date_debut,$date_fin){
        
        /*$result=DB::table('durations')
            ->whereBetween('created_at', [$date_debut, $date_fin])
            ->select( DB::raw('AVG(time) as time'))
            ->first();
        return number_format($result->time/60, 2, ':', '');*/
        $avgs=$this->filtreSessionTypeDate($date_debut,$date_fin,'M','AVG(time) as total')['y'];
        if(count($avgs)){
            return number_format(array_sum($avgs)/count($avgs), 2, ':', '');
        }else{
            return '00:00';
        }
    }

    public function getDurationByTheme($date_debut,$date_fin,$period){
        $stat=array();
        $stat['total']=0;
        $stat['chart']=array();
        $result=DB::table('durations')
            ->whereBetween('created_at', [$date_debut, $date_fin])
            ->select(DB::raw('DATE(created_at) as cdate'), DB::raw('AVG(time) as time'))
            ->groupBy('cdate')
            ->get();
        $stat['total']=$result->sum('time');
        $resultArr= $result->pluck('time','cdate')->toArray();
        //if(count($period) != count($resultArr)){
            foreach ($resultArr as $key => $value) {
                $period[$key]=floatval(number_format($value/60, 2, '.', ''));
            }
            $stat['chart']= array_values($period);
       // }else{
          //  $stat['chart']=array_values($resultArr);
        //}
        return $stat;
    }

    public function sharedPost($id){
        $post=Post::where('id',$id)->update(['share'=>1]);
        return $post;
    }

    public function filterChart(Request $request){
        /*$result=DB::table('clients')
            ->whereBetween('created_at', ["2019-01-01", "2023-01-01"])
            ->select(DB::raw('created_at as hour'), DB::raw('count(id) as total'))
            ->groupBy('hour')
            ->get();
            dd($result->groupBy(function($reg){
                return date('H',strtotime($reg->hour));
            }));*/

        /*$result=DB::table('clients')
            ->whereBetween('created_at', ["2019-01-01", "2023-01-01"])
            ->select(DB::raw('created_at as hour'), DB::raw('count(id) as total'))
            ->groupBy('hour')
            ->get();
            dd($result->groupBy(function($reg){
                return date('W',strtotime($reg->hour));
            }));*/


            /*$result=DB::table('clients')
            ->whereBetween('created_at', ["2019-01-01", "2023-01-01"])
            ->select(DB::raw('created_at as hour'), DB::raw('count(id) as total'))
            ->groupBy('hour')
            ->get();
            dd($result->groupBy(function($reg){
                return date('M',strtotime($reg->hour));
            })); */
            /*$result=DB::table('clients')
            ->whereBetween('created_at', ["2019-01-01", "2023-01-01"])
            ->select(DB::raw('created_at as hour'), DB::raw('count(id) as total'))
            ->groupBy('hour')
            ->get();
            dd($result->groupBy(function($reg){
                return date('d-m-Y',strtotime($reg->hour));
            }));
        return $result; */
        switch ($request->chartId) {
            case '1':
              //return $this->filtreTypeDate($request->date_debut,$request->date_fin,$request->filter,'clients','updated_at');
              return $this->filtreUnicUser($request->date_debut,$request->date_fin,$request->filter);
              break;
            case '2':
              return $this->filtreActiveUser($request->date_debut,$request->date_fin,$request->filter);
              break;
            case '3':
              return $this->filtreTypeDate($request->date_debut,$request->date_fin,$request->filter,'clients','created_at');
              break;
            case '4':
              return $this->filtreSessionTypeDate($request->date_debut,$request->date_fin,$request->filter,'count(*) as total');
              break;
            case '5':
            return $this->filtreSessionTypeDate($request->date_debut,$request->date_fin,$request->filter,'AVG(time) as total');
              break;
            case '6':
              return $this->filtreTypeDate($request->date_debut,$request->date_fin,$request->filter,'posts','created_at');
              break;
            case '7':
              return $this->filtreTypeDate($request->date_debut,$request->date_fin,$request->filter,'likes','updated_at');
              break;
            case '8':
              return $this->filtreTypeDate($request->date_debut,$request->date_fin,$request->filter,'comments','created_at');
              break;
            case '9':
              return $this->filtreTypeDate($request->date_debut,$request->date_fin,$request->filter,'likecs','updated_at');
              break;
            case '11':
              return $this->filterGlobalChart($request);
              break;
        }

    }
    public function filtreTypeDate($date_debut,$date_fin,$filter,$table,$column){
        $stat=array();
        $chart=array();
        $result=DB::table($table)
            ->whereBetween($column, [$date_debut, $date_fin." 23:59:59"]);
        if($table=='posts' || $table=='comments')
            $result=$result->where('is_deleted',0)->where('state','acceptee');
        if($table=='likes'|| $table=='likecs')
            $result=$result->where('dislike',0);

        $result=$result->select(DB::raw($column.' as date'), DB::raw('count(*) as total'))
            ->orderBy($column,'ASC')
            ->groupBy('date')
            ->get();
        $filterResult=$result->groupBy(function($reg) use ($filter){
                return date($filter,strtotime($reg->date));
            });
        foreach ($filterResult as $key => $coll) {
            $stat[$key]=$coll->sum('total');
        }
        if($filter=="Y-m-d H:00" || $filter=="Y-m-d") $stat=$this->filterHourOrDay($filter,$stat,$date_debut,$date_fin);
        $chart['x']=array_keys($stat);
        $chart['y']=array_values($stat);
        return $chart;
    }

    public function filtreActiveUser($date_debut,$date_fin,$filter){
        $stat=array();
        $chart=array();
        /*$result = DB::select('SELECT count(*) as total ,created_at as date from clients where id
             IN (SELECT client_id FROM posts WHERE created_at between "'.$date_debut.'" AND "'.$date_fin.'")
             OR
             id IN (SELECT client_id FROM likes WHERE created_at between "'.$date_debut.'" AND "'.$date_fin.'") GROUP BY date ORDER BY created_at');*/
        if($filter=="Y-m-d H:00"){
            $sql="select count(distinct client_id) as total, S.date from
                (
                    (select L.client_id, date_format(L.created_at, '%Y-%m-%d %H:00') as date from likes L group by client_id, date_format(L.created_at, '%Y-%m-%d %H:00'))
                    union
                    (select P.client_id, date_format(P.created_at, '%Y-%m-%d %H:00') as date from posts P group by client_id, date_format(P.created_at, '%Y-%m-%d %H:00'))
                )
                as S where S.date >= '".$date_debut."' AND S.date <= '".$date_fin." 23:59:59' group by S.date";
                }else{
            $sql="select count(distinct client_id) as total, S.date from
                (
                    (select L.client_id, date_format(L.created_at, '%Y-%m-%d') as date from likes L group by client_id, date_format(L.created_at, '%Y-%m-%d'))
                    union
                    (select P.client_id, date_format(P.created_at, '%Y-%m-%d') as date from posts P group by client_id, date_format(P.created_at, '%Y-%m-%d'))
                )
                as S where S.date >= '".$date_debut."' AND S.date <= '".$date_fin."' group by S.date";
        }
        $result = DB::select($sql);

        $result=collect($result);
        $filterResult=$result->groupBy(function($reg) use ($filter){
                return date($filter,strtotime($reg->date));
            });
        foreach ($filterResult as $key => $coll) {
            $stat[$key]=$coll->sum('total');
        }
        if($filter=="Y-m-d H:00" || $filter=="Y-m-d") $stat=$this->filterHourOrDay($filter,$stat,$date_debut,$date_fin);
        $chart['x']=array_keys($stat);
        $chart['y']=array_values($stat);
        return $chart;
    }
    
    public function filtreUnicUser($date_debut,$date_fin,$filter){
        $stat=array();
        $chart=array();
        /*$result = DB::select('SELECT count(*) as total ,created_at as date from clients where id
             IN (SELECT client_id FROM posts WHERE created_at between "'.$date_debut.'" AND "'.$date_fin.'")
             OR
             id IN (SELECT client_id FROM likes WHERE created_at between "'.$date_debut.'" AND "'.$date_fin.'") GROUP BY date ORDER BY created_at');*/
        if($filter=="Y-m-d H:00"){
            $sql="select count(distinct client_id) as total, S.date from
                
                    (select L.client_id, date_format(L.created_at, '%Y-%m-%d %H:00') as date from durations L group by client_id, date_format(L.created_at, '%Y-%m-%d %H:00'))
                    
                as S where S.date >= '".$date_debut."' AND S.date <= '".$date_fin." 23:59:59' group by S.date";
                }else{
            $sql="select count(distinct client_id) as total, S.date from

                    (select L.client_id, date_format(L.created_at, '%Y-%m-%d') as date from durations L group by client_id, date_format(L.created_at, '%Y-%m-%d'))
                    
                as S where S.date >= '".$date_debut."' AND S.date <= '".$date_fin."' group by S.date";
        }
        $result = DB::select($sql);

        $result=collect($result);
        $filterResult=$result->groupBy(function($reg) use ($filter){
                return date($filter,strtotime($reg->date));
            });
        foreach ($filterResult as $key => $coll) {
            $stat[$key]=$coll->sum('total');
        }
        if($filter=="Y-m-d H:00" || $filter=="Y-m-d") $stat=$this->filterHourOrDay($filter,$stat,$date_debut,$date_fin);
        $chart['x']=array_keys($stat);
        $chart['y']=array_values($stat);
        return $chart;
    }


    public function filtreSessionTypeDate($date_debut,$date_fin,$filter,$type){
        $stat=array();
        $chart=array();
        $result=DB::table('durations')
            ->whereBetween('created_at', [$date_debut, $date_fin." 23:59:59"])
            ->select(DB::raw('created_at as cdate'), DB::raw($type))
            ->orderBy('created_at','ASC')
            ->groupBy('cdate')
            ->get();
        $filterResult=$result->groupBy(function($reg) use ($filter){
                return date($filter,strtotime($reg->cdate));
            });
        if($type=='count(*) as total'){
            foreach ($filterResult as $key => $coll) {
                $stat[$key]=$coll->sum('total');
            }
        }else{
            foreach ($filterResult as $key => $coll) {
                $stat[$key]=floatval(number_format(($coll->sum('total')/$coll->count())/60, 2, '.', ''));
            }
        }
        if($filter=="Y-m-d H:00" || $filter=="Y-m-d") $stat=$this->filterHourOrDay($filter,$stat,$date_debut,$date_fin);
        $chart['x']=array_keys($stat);
        $chart['y']=array_values($stat);

           return $chart;
    }

    public function filterHourOrDay($type,$data,$date_debut,$date_fin){
        if ($type=="Y-m-d H:00") {
            $period=$this->getDateTimePeriode($date_debut,$date_fin);
        }else{//"Y-m-d"
            $period=$this->getDatePeriode($date_debut,$date_fin);
        }
        foreach ($data as $key => $value) {
            if(isset($period[$key]))
                $period[$key]=$value;
        }
        return $period;
    }

    public function getNbPostByTheme($themes){
        $result['themes']=$themes->pluck('title')->toArray();
        $result['series']=array();$result['series'][0] = new \stdClass();$result['series'][1] = new \stdClass();
        $result['series'][0]->name= "Refusée"; $result['series'][0]->data= array();$result['series'][0]->color="#f02222";
        $result['series'][1]->name= "Acceptée"; $result['series'][1]->data= array();$result['series'][1]->color="#4caf50";
        foreach ($themes as $theme) {
            array_push($result['series'][0]->data,$theme->posts->where('state','refusee')->count());
            array_push($result['series'][1]->data,$theme->posts->where('state','acceptee')->count());
        }
        return $result;
    }
    public function filterGlobalChart($request){
        $result=array();
        $result['y']=array();
        array_push($result['y'],
                   $this->getObjectSerie($this->filtreTypeDate($request->date_debut,$request->date_fin,$request->filter,'clients','updated_at'),
                   'Utilisateur uniques',
                   "#b0cff4"));
        array_push($result['y'],
                   $this->getObjectSerie($this->filtreActiveUser($request->date_debut,$request->date_fin,$request->filter),
                    'Utilisateur actifs',
                    "#887dff"
               ));
        array_push($result['y'],
                   $this->getObjectSerie($this->filtreTypeDate($request->date_debut,$request->date_fin,$request->filter,'clients','created_at'),
                    'Nouveaux utilisateurs',
                    "#f986c7"
               ));
        array_push($result['y'],
                   $this->getObjectSerie($this->filtreSessionTypeDate($request->date_debut,$request->date_fin,$request->filter,'count(*) as total'),
                    'Nombre de sessions',
                    "#9cefc7"));
        /*array_push($result['y'],
                   $this->getObjectSerie($this->filtreSessionTypeDate($request->date_debut,$request->date_fin,$request->filter,'sum(time) as total')));5*/
        $aboutPosts=$this->filtreTypeDate($request->date_debut,$request->date_fin,$request->filter,'posts','created_at');
        $result['x']=$aboutPosts['x'];


        array_push($result['y'],
                   $this->getObjectSerie($this->filtreTypeDate($request->date_debut,$request->date_fin,$request->filter,'likes','created_at'),
                   'Total likes',
                    "#7f7f7f"));

        array_push($result['y'],
                   $this->getObjectSerie($aboutPosts,
                   'Total posts',
                   "#fda7a4"));
        //dd($result);
        return $result;
    }

    public function getObjectSerie($data,$name,$color){
        $obj=new \stdClass();
        $obj->name=$name;
        $obj->color=$color;
        $obj->data=$data['y'];
        return $obj;
    }
        //\DB::enableQueryLog();
 }       //dd(\DB::getQueryLog());
