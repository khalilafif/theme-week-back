<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Client;
use App\Likec;
use App\Post;
class CommentController  extends Controller
{
    public function index(){
    	$comments=Comment::where('is_deleted',0)->with('client')->orderBy('created_at','DESC')->
                           withCount(['likecs'=>function($query){
                                $query->where('dislike',0);
                            }])->get();
    	$waitingCom=$comments->where('state','en attente');
    	$otherCom=$comments->where('state', '!=' ,'en attente');
    	//dd($comments,$waitingCom,$otherCom);
    	return view('vendor/voyager/comments/browse',compact('waitingCom','otherCom')); 
    }
    public function list($id){
        $comments=Comment::where('post_id',$id)->
                           where('is_deleted',0)->
                           where('state','acceptee')->
                           with('client')->
                           withCount(['likecs'=>function($query){
                                $query->where('dislike',0);
                            }])->
                           orderBy('created_at','ASC')->get();
        return view('vendor/voyager/comments/list',compact('comments'));
    }
    public function deleteCommentPost($type,$id){
        if($type==1){
            $data=Post::findOrFail($id);
        }else if($type ==2){
            $data=Comment::findOrFail($id);
        }else{
            abort(404);
        }
        $data->update(['admin_deleted' => 1]);
        return redirect()->back();
    }
    public function retailerComment($id){
        $comments=Comment::where('client_id',$id)->
                           //where('is_deleted',0)->
                           //where('state','acceptee')->
                           where('state', '!=' ,'en attente')->
                           with('client')->
                           withCount(['likecs'=>function($query){
                                $query->where('dislike',0);
                            }])->
                           orderBy('created_at','ASC')->get();
        return view('vendor/voyager/comments/list',compact('comments'));
    }

    public function edit($id){
    	$comment=Comment::where('id',$id)->with('client')->with('post')->first();
    	return view('vendor/voyager/comments/edit-add',compact('comment')); 
    }

    public function validation(Request $request){
    	$this->validate($request,[
            'comment_id'=>'required',
            'state'=>'required'
        ]);
        $comment=Comment::findOrFail($request->comment_id);
        $comment->state=$request->state;
        if($request->state=="refusee")
            $comment->reason=$request->reason;        
        $comment->update();
        return redirect()->back();
    }
    public function LikesComment($id){        
        $likes=Likec::where('dislike',0)->where('comment_id',$id)->with('client')->get();
        return view('vendor/voyager/likes/list',compact('likes'));   
    }
    /* API function */
    public function getCustomer($user){
		$client=null;
		if(isset($user["UserId"]) && is_null($client=Client::where('userid',$user["UserId"])->first())){
			$client= new Client();
			$client->userid=(isset($user["UserId"])) ? $user["UserId"] : null ;
			$client->contactid=(isset($user["ContactId"])) ? $user["ContactId"] : null ;
			$client->firstname=(isset($user["FirstName"])) ? $user["FirstName"] : null ;
			$client->lastname=(isset($user["LastName"])) ? $user["LastName"] : null ;
			$client->role=(isset($user["Role"])) ? $user["Role"] : null ;
			$client->email=(isset($user["Email"])) ? $user["Email"] : null ;
			$client->language=(isset($user["Language"])) ? $user["Language"] : null ;
			$client->sessionid=(isset($user["SessionId"])) ? $user["SessionId"] : null ;
			$client->touchpointids=(isset($user["TouchpointIDs"]));
			$client->tradeprogram=(isset($user["TouchpointIDs"])) ? $user["TouchpointIDs"] : null ;
			$client->reasontype=(isset($user["ReasonType"])) ? $user["ReasonType"] : null ;
			$client->awardpoints=(isset($user["AwardPoints"])) ? $user["AwardPoints"] : null ;
			$client->reasondescription=(isset($user["ReasonDescription"])) ? $user["ReasonDescription"] : null ;
			$client->mailingcity=(isset($user["MailingCity"])) ? $user["MailingCity"] : null ;
			$client->mobilephone=(isset($user["MobilePhone"])) ? $user["MobilePhone"] : null ;
			$client->origin=(isset($user["Origin"])) ? $user["Origin"] : null ;
			$client->save();
		}else{
			$client->sessionid=(isset($user["SessionId"])) ? $user["SessionId"] : null ;
            $client->awardpoints=(isset($user["AwardPoints"])) ? $user["AwardPoints"] : null ;
            $client->role=(isset($user["Role"])) ? $user["Role"] : null ;
			$client->email=(isset($user["Email"])) ? $user["Email"] : null ;
			$client->mailingcity=(isset($user["MailingCity"])) ? $user["MailingCity"] : null ;
            $client->update();
		}
		return $client;
	}

	public function storeComment(Request $request){
        if(isset($request->content) && isset($request->post_id)){
            $client = $this->getCustomer(\Request::get('retailer'));
            $comment=new Comment();
            $comment->content=$request->content;
            $comment->client_id=$client->id;
            $comment->post_id=$request->post_id;
            $comment->save();
            return response(['status'=>'success','message'=>'comment saved','data'=>'saved'], 200); 
        }else{
            return response(['status'=>'success',"data"=>'not saved','message'=>'data not valid'], 200);
        }        
    }

    public function updateComment(Request $request){
        if(isset($request->content) && isset($request->comment_id)){
            $client = $this->getCustomer(\Request::get('retailer'));
            $comment=Comment::findOrFail($request->comment_id);
            if($comment->client_id==$client->id){
            	$comment->content=$request->content;	            
            	$comment->state="en attente";	            
	            $comment->update();
	            return response(['status'=>'success','message'=>'comment saved','data'=>'saved'], 200); 	
            }else{
            	return response(['status'=>'success',"data"=>'not updated','message'=>'data not valid'], 200);
            }            
        }else{
            return response(['status'=>'success',"data"=>'not saved','message'=>'data not valid'], 200);
        }        
    }

    public function deleteComment(Request $request){
        if(isset($request->comment_id)){
            $client = $this->getCustomer(\Request::get('retailer'));
            $comment=Comment::findOrFail($request->comment_id);
            if($comment->client_id==$client->id){
            	$comment->is_deleted=1;	            
	            $comment->update();
	            return response(['status'=>'success','message'=>'comment deleted','data'=>'saved'], 200); 	
            }else{
            	return response(['status'=>'success',"data"=>'not deleted','message'=>'data not valid'], 200);
            }            
        }else{
            return response(['status'=>'success',"data"=>'not deleted','message'=>'data not valid'], 200);
        }        
    }
    public function editComment(Request $request){
        if(isset($request->comment_id)){
        	$data=array();
            $client = $this->getCustomer(\Request::get('retailer'));
            $comment=Comment::findOrFail($request->comment_id);
            if($comment->client_id==$client->id){
            	$data['id']=$comment->id;
            	$data['content']=$comment->content;
	            return response(['status'=>'success','message'=>'comment showed','data'=>$data], 200); 	
            }else{
            	return response(['status'=>'success',"data"=>'not deleted','message'=>'data not valid'], 200);
            }            
        }else{
            return response(['status'=>'success',"data"=>'not deleted','message'=>'data not valid'], 200);
        }        
    }
    public function listComments(Request $request){        
        if(isset($request->post_id)){
        	$comments=Comment::where('post_id',$request->post_id)->
        					  where('is_deleted',0)->
        					  where('state',"acceptee")->
        					  with('client')->
        					  get();
        
        	return response(['status'=>'success','message'=>'comment list','data'=>$comments], 200);
        }else{
            return response(['status'=>'success',"data"=>null,'message'=>'data not valid'], 200);
        }       
    }
    public function myComments(Request $request){        
        $client = $this->getCustomer(\Request::get('retailer'));
        $comments=Comment::where('client_id',$client->id)->
        					  where('is_deleted',0)->
        					  with('post')->
        					  get();
        
        return response(['status'=>'success','message'=>'comment list','data'=>$comments], 200);       
    }
    /*** for Like Comment API ***/
    public function storeLike(Request $request)
    {
        if(isset($request->comment_id)){            
            $client = $this->getCustomer(\Request::get('retailer'));
            $oldLike=Likec::where('client_id',$client->id)->where('comment_id',$request->comment_id)->first();
            if(empty($oldLike)){                
                $like=new Likec();
                $like->client_id=$client->id;
                $like->comment_id=$request->comment_id;
                $like->dislike=0;
                $like->save();
                return response(['status'=>'success','message'=>'Comment like saved','data'=>['dislike'=>0]], 200);
            }else{
                if($oldLike->dislike){
                    $oldLike->dislike=0;
                    $oldLike->update();
                    return response(['status'=>'success','message'=>'like','data'=>['dislike'=>0]], 200);
                }else{
                    $oldLike->dislike=1;
                    $oldLike->update();
                    return response(['status'=>'success','message'=>'dislike','data'=>['dislike'=>1]], 200);
                } 
            }
        }else{
            return response(['status'=>'success','message'=>'data not valid','data'=>false], 200);
        }
        
    }
    public function listLikes(Request $request){
        if(isset($request->comment_id)){            
            $likes= Likec::where('dislike',0)->where('comment_id',$request->comment_id)->with('client')->orderByDesc('created_at')->get();
            return response(['status'=>'success','message'=>'comment like list','data'=>$likes], 200);
        }else{
            return response(['status'=>'success','message'=>'data not valid','data'=>false], 200);
        }
    }
    /* End API function */
}
