<?php

namespace App\Http\Controllers;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\Duration;
use App\Client;
class DurationController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
     public function getCustomer($user){
		$client=null;
		if(isset($user["UserId"]) && is_null($client=Client::where('userid',$user["UserId"])->first())){
			$client= new Client();
			$client->userid=(isset($user["UserId"])) ? $user["UserId"] : null ;
			$client->contactid=(isset($user["ContactId"])) ? $user["ContactId"] : null ;
			$client->firstname=(isset($user["FirstName"])) ? $user["FirstName"] : null ;
			$client->lastname=(isset($user["LastName"])) ? $user["LastName"] : null ;
			$client->role=(isset($user["Role"])) ? $user["Role"] : null ;
			$client->email=(isset($user["Email"])) ? $user["Email"] : null ;
			$client->language=(isset($user["Language"])) ? $user["Language"] : null ;
			$client->sessionid=(isset($user["SessionId"])) ? $user["SessionId"] : null ;
			$client->touchpointids=(isset($user["TouchpointIDs"]));
			$client->tradeprogram=(isset($user["TouchpointIDs"])) ? $user["TouchpointIDs"] : null ;
			$client->reasontype=(isset($user["ReasonType"])) ? $user["ReasonType"] : null ;
			$client->awardpoints=(isset($user["AwardPoints"])) ? $user["AwardPoints"] : null ;
			$client->reasondescription=(isset($user["ReasonDescription"])) ? $user["ReasonDescription"] : null ;
			$client->mailingcity=(isset($user["MailingCity"])) ? $user["MailingCity"] : null ;
			$client->mobilephone=(isset($user["MobilePhone"])) ? $user["MobilePhone"] : null ;
			$client->origin=(isset($user["Origin"])) ? $user["Origin"] : null ;
			$client->save();
		}else{
			$client->sessionid=(isset($user["SessionId"])) ? $user["SessionId"] : null ;
            $client->awardpoints=(isset($user["AwardPoints"])) ? $user["AwardPoints"] : null ;
            $client->update();
		}
		return $client;
	}
 
    /*** for API ***/
    public function storeDuration(Request $request)
    {
        if(isset($request->time) && isset($request->theme_id) ){
            $client = $this->getCustomer(\Request::get('retailer'));
            $duration=new Duration();
            $duration->time=$request->time;
            $duration->date=date("Y-m-d");   
            $duration->close=$request->close;      
            $duration->client_id=$client->id;
            $duration->theme_id=$request->theme_id;
            $duration->save();
            return response(['status'=>'success','message'=>'duration saved','data'=>'saved'], 200); 
        }else{
            return response(['status'=>'success',"data"=>'not saved','message'=>'data not valid'], 200);
        }        
    }
}
