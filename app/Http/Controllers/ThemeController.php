<?php

namespace App\Http\Controllers;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\Theme;
use App\Post;
use App\Like;
use App\Client;
class ThemeController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $themes = Theme::with('posts')->get();         
        return view('vendor/voyager/themes/browse',compact('themes')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vendor/voyager/themes/edit-add'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[ 
            'datedebut'=>'required|date',
            'datefin'=>'required|date',
            'title'=>'required']);
    	if(strtotime($request->datedebut) >= strtotime($request->datefin)){
			return redirect()->back()->withErrors('Date invalide');
		}

      	if(!is_null(Theme::where('date_begin',$request->datedebut)->where('date_end',$request->datefin)->first())){
            return redirect()->back()->withErrors('Ce theme existe');
        } 
    	$theme= new Theme();
    	$theme->date_begin = $request->datedebut;
    	$theme->date_end = $request->datefin;
        $theme->title = $request->title;
    	$theme->subject= $request->subject;
    	$theme->save();
       
       return redirect('admin/themes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $theme=Theme::findOrFail($id);
    	return view('vendor/voyager/themes/edit-add',compact('theme')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request,[ 
            'datedebut'=>'required|date',
            'datefin'=>'required|date',
            'title'=>'required']);
    	if(strtotime($request->datedebut) >= strtotime($request->datefin)){
			return redirect()->back()->withErrors('Date invalide');
        }
        
      	$theme=Theme::findOrFail($request->theme_id);       
        $theme->date_begin = $request->datedebut;
        $theme->date_end = $request->datefin;
        $theme->title = $request->title;
        $theme->subject= $request->subject;
    	$theme->update();
       
        return redirect('admin/themes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $theme=Theme::where('id',$id)->with('posts')->get();
        $theme=$theme->first();
        foreach($theme->posts as $post){
            $post->likes()->delete();
            $post->delete();
        }
        $theme->delete();
        return redirect()->back();
    }
    /*** for API ***/
    public function getAllThemes(Request $request){
        $themes=Theme::all();
        $data['retailer']=$this->getClientAtt(\Request::get('retailer'));
        $data['themes']=$themes;
        $data['posts']=[];
        $data['theme']=$themes->last();
        if(!empty($data['theme'])){ 
            $data['posts']=Post::
                where('theme_id',$data['theme']->id)->
                where('is_deleted',0)->
                where('state','acceptee')->
                with(['likes'=>function($query){
                    $query->where('dislike',0);}])->
                with(['comments'=>function($query){
                    $query->where('is_deleted',0)->
                            where('state','acceptee')->
                            with('client')->
                            with(['likecs'=>function($query1){
                                $query1->where('dislike',0);}]);
                    }])->
                with('client')->orderByDesc('created_at')->get();
        }
        return response(['status'=>'success','message'=>'themes list','data'=>$data], 200);   
    }
    public function getClientAtt($retailer){
        $client = $this->getCustomer($retailer);
        $result['id']=$client->id;
        $result['firstname']=$client->firstname;
        $result['lastname']=$client->lastname;
        return $result;
    }
    public function getCustomer($user){
		$client=null;
		if(isset($user["UserId"]) && is_null($client=Client::where('userid',$user["UserId"])->first())){
			$client= new Client();
			$client->userid=(isset($user["UserId"])) ? $user["UserId"] : null ;
			$client->contactid=(isset($user["ContactId"])) ? $user["ContactId"] : null ;
			$client->firstname=(isset($user["FirstName"])) ? $user["FirstName"] : null ;
			$client->lastname=(isset($user["LastName"])) ? $user["LastName"] : null ;
			$client->role=(isset($user["Role"])) ? $user["Role"] : null ;
			$client->email=(isset($user["Email"])) ? $user["Email"] : null ;
			$client->language=(isset($user["Language"])) ? $user["Language"] : null ;
			$client->sessionid=(isset($user["SessionId"])) ? $user["SessionId"] : null ;
			$client->touchpointids=(isset($user["TouchpointIDs"]));
			$client->tradeprogram=(isset($user["TouchpointIDs"])) ? $user["TouchpointIDs"] : null ;
			$client->reasontype=(isset($user["ReasonType"])) ? $user["ReasonType"] : null ;
			$client->awardpoints=(isset($user["AwardPoints"])) ? $user["AwardPoints"] : null ;
			$client->reasondescription=(isset($user["ReasonDescription"])) ? $user["ReasonDescription"] : null ;
			$client->mailingcity=(isset($user["MailingCity"])) ? $user["MailingCity"] : null ;
			$client->mobilephone=(isset($user["MobilePhone"])) ? $user["MobilePhone"] : null ;
			$client->origin=(isset($user["Origin"])) ? $user["Origin"] : null ;
			$client->save();
		}else{
			$client->sessionid=(isset($user["SessionId"])) ? $user["SessionId"] : null ;
		}
		return $client;
	}
}
