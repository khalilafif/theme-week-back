<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Post;
use App\Comment;
class RetailerController extends Controller
{
    public function index(){
       $clients=$this->getClients();
       return view('vendor/voyager/clients/browse',compact('clients'));
    }
    public function getClients($id=null){
         $clients=Client::with(['posts'=>function($query){
                            $query->where('state','!=','en attente')->with(['likes'=>function($query1){
                                $query1->where('dislike',0);
                            }]);
                        }])->
                        with(['likes'=>function($query2){
                                $query2->where('dislike',0);
                            }])->
                        with(['comments'=>function($query3){
                                $query3->where('state','!=','en attente');
                            }]);
        if($id)
            $clients=$clients->where('id',$id);
        $clients=$clients->get();
        foreach($clients as $client){
            $collected=0;
            foreach($client->posts as $post){
                $collected+=count($post->likes);
            }
            $client->like_collected=$collected;
            $ps=$this->getAllStats($client->posts);
            $client->p_acceptee=$ps['acceptee'];
            $client->p_refusee=$ps['refusee'];
            $client->p_deleted=$ps['deleted'];
            $cs=$this->getAllStats($client->comments);
            $client->c_acceptee=$cs['acceptee'];
            $client->c_refusee=$cs['refusee'];
            $client->c_deleted=$cs['deleted'];
        }
        return $clients;
    }
    public function retailerFilter($model,$type,$id){
            if($model==1){
                $data=Post::where('client_id',$id);
            }else if($model ==2){
                $data=Comment::where('client_id',$id);
            }else{
                abort(404);
            }
            if($type==1){
                $data=$data->where('state','refusee')->where('is_deleted',0);
            }else if($type==2){
                $data=$data->where('state','acceptee')->where('is_deleted',0);
            }else if($type==3){
                $data=$data->where('state','!=','en attente')->where('is_deleted',1);
            }else{
                abort(404);
            }
            if($model==1){
                    $posts= $data->with('theme')->
                                   with('user')->
                                   with('client')->
                                   withCount(['likes'=>function($query){
                                      $query->where('dislike',0);
                                   }])->withCount(['comments'=>function($query){
                                      $query->where('is_deleted',0)
                                            ->where('state','acceptee');
                                   }])->orderByDesc('created_at')->
                                   get();

                    $themes=\App\Theme::all();
                    $topPost=$this->getTopPostByClient($id);
                     return view('vendor/voyager/posts/browse',compact('posts','themes','topPost'));
            }else if($model ==2){
                     $comments= $data->with('client')->
                                       withCount(['likecs'=>function($query){
                                          $query->where('dislike',0);
                                        }])->
                                       orderBy('created_at','ASC')->
                                       get();
                      return view('vendor/voyager/comments/list',compact('comments'));
            }

    }
    public function getTopPostByClient($client_id){
        $post=Post::where('state','acceptee')->
            where('is_deleted',0)->where('client_id',$client_id)->withCount('likes')->orderBy('likes_count', 'desc')->first();
        return $post;
    }

    public function getAllStats($list){
        $res=array();
        $res['acceptee']=$list->where('state',"acceptee")->where('is_deleted',0)->count();
        $res['refusee']=$list->where('state',"refusee")->where('is_deleted',0)->count();
        $res['deleted']=$list->where('is_deleted',1)->count();
        return $res;
    }

    public function list($id){
        $clients=$this->getClients($id);   
        return view('vendor/voyager/clients/browse',compact('clients'));
    }
}
