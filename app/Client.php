<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    public function likes(){
        return $this->hasMany('App\Like');
    } 
    public function posts(){
        return $this->hasMany('App\Post');
    }
    public function comments(){
        return $this->hasMany('App\Comment');
    }
}
