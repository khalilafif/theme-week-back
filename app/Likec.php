<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Likec extends Model
{
    public function comment(){
        return $this->belongsTo('App\Comment');
    }
    public function client(){
        return $this->belongsTo('App\Client');
    }
}
