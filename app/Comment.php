<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Comment extends Model
{
    protected $fillable = [
        'admin_deleted'
    ];
    public function newQuery($excludeDeleted = true) {
        return parent::newQuery($excludeDeleted)
            ->where('admin_deleted', '=', 0);
    }
    public function post(){
        return $this->belongsTo('App\Post');
    }
    public function client(){
        return $this->belongsTo('App\Client');
    }
    public function likecs(){
        return $this->hasMany('App\Likec');
    }
}
