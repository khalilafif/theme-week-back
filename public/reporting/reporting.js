  var scrollObj= {
            enabled: true,
            barBackgroundColor: "#dbdcde",
            rifleColor: false,
            barBorderRadius: 7,
            buttonBorderRadius: 7,
            buttonBackgroundColor: "#dbdcde"
        };
$( document ).ready(function() {
  var uniUserC=null;
  var actiUserC=null;
  var newRC=null;
  var sessDC=null;
  var sessDCD=null;  
  var postC=null;
  var commentC=null;
  var likecsC=null;
  var likesC=null;
  var global_chart=null;
  if($('#global-chart-bloc').length>0){
    uniqueUserChart();
    activeUserChart();
    postsChart();
    commentsChart();
    likecsChart();
    likesChart();
    sessionDurationChart();
    newRetailerChart();
    sessionDurationDateChart();
    pieRegion();
    nbPostByThme();
    globalChart();
  }
});

/*$('.theme-top-post').on('change', function() {  
  $('#share-post').attr('data-id',"");  
  $('.sha-txt').text('Share');
  $('#share-post').removeClass('shared');
  getTopPost(this.value,null,null);
});*/

$('#global-theme').on('change', function() {
    $('#inp-debut').val(theme_begins[this.value]);
    $('#inp-fin').val(theme_ends[this.value]);
});

$('#share-post').click(function() {  
  let postId=$(this).attr('data-id');
  if(postId) postShared(postId);
});
$('.filter').click(function() {  
  let id=$(this).attr('data-id');
  let type=$(this).attr('data-type');
  $(this).parent().find(".active").removeClass("active");
  $(this).addClass('active');
  filterChart(type,id);
});
$('.date-range-filter').change(function() {
  let theme= ($('.theme-filter').val() == "") ? null : themes_id[$('.theme-filter').val()] ;
  let date_debut=($('#date-debut').val() == "") ? null : $('#date-debut').val() ;
  let date_fin=($('#date-fin').val()== "") ? null : $('#date-fin').val() ;
  if(date_debut && date_fin) getTopPost(theme,date_debut,date_fin);
  console.log(theme,date_debut,date_fin);
});
$('.theme-filter').change(function(e){
  let theme= ($(this).val() == "") ? null : themes_id[$(this).val()] ;
  let date_debut=($('#date-debut').val() == "") ? null : $('#date-debut').val() ;
  let date_fin=($('#date-fin').val()== "") ? null : $('#date-fin').val() ;
  getTopPost(theme,date_debut,date_fin);
});
function getTopPost(themsId,date_begin,date_end){
 
 $.ajax({
  type: "POST",
  url: routeTopPost,
  data:{
    _token:token,
    theme_id:themsId,
    date_begin: date_begin,  
    date_end: date_end
  },
  success: function(response){   
      console.log(response);
      if(response){
          $('#share-post').attr('data-id',response.id);
          $('#post-title').html(response.title+'<span class="nb-likes">Likes('+response.likes_count+')</span>');
          $('#post-description').text(response.description);
          if(response.media){
            $('#post-media').attr('src',response.media);
            $('#post-media').removeClass('hidden');
          }else{
            $('#post-media').addClass('hidden');
          }
          if(response.share){
            $('#share-post').addClass('shared');
            $('.sha-txt').text('Shared');
          }
          $('.bloc-top-post').removeClass('hidden');
          $('.top-post-empty').addClass('hidden');
    }else{
      $('.bloc-top-post').addClass('hidden');
      $('.top-post-empty').removeClass('hidden');
    }
  },error(e) {
   console.log(e);
   if (e && e.status==408) getTopPost(themsId,date_begin,date_end);   
  }
 }); 
}

function postShared(postId){
 
 $.ajax({
  type: "GET",
  url: routeSharePost.substring(0, routeSharePost.length - 1)+postId,
  success: function(response){   
  console.log(response);
      if(response){
        $('#share-post').addClass('shared');
        $('.sha-txt').text('Shared');
      }

  },error(e) {
   console.log(e);
   if (e && e.status==408) postShared(postId);
  }
 }); 
}

function filterChart(filter,id){
 console.log(filter,
id);
 $.ajax({
  type: "POST",
  data:{
    chartId: id,
    date_debut:date_debut,
    date_fin:date_fin,
    filter: filter,
    _token: token
  },
  url: routeFilterChart,
  success: function(response){   
  console.log(response);
      if(response){
        updateChart(id,response);
      }

  },error(e) {
   console.log(e);
   if (e && e.status==408) postShared(postId);
  }
 }); 
}

function updateChart(id,data){
  console.log(id,data);
  switch (id) {
    case '1':
      if(data.x.length<=7){uniUserC.xAxis[0].setExtremes(0, data.x.length-1);        }
      else{ uniUserC.xAxis[0].setExtremes(0, 7); }
      uniUserC.update(checkScroll(data));
      break;
    case '2':
      if(data.x.length<=7){actiUserC.xAxis[0].setExtremes(0, data.x.length-1);}
      else{ actiUserC.xAxis[0].setExtremes(0, 7);}
      actiUserC.update(checkScroll(data));
      break;
    case '3':
      if(data.x.length<=7){newRC.xAxis[0].setExtremes(0, data.x.length-1);}
      else{ newRC.xAxis[0].setExtremes(0, 7);}
      newRC.update(checkScroll(data));
      break;
    case '4':
      if(data.x.length<=7){sessDCD.xAxis[0].setExtremes(0, data.x.length-1);}
      else{ sessDCD.xAxis[0].setExtremes(0, 7);}
      sessDCD.update(checkScroll(data));
      break;
    case '5':
      if(data.x.length<=7){sessDC.xAxis[0].setExtremes(0, data.x.length-1);}
      else{ sessDC.xAxis[0].setExtremes(0, 7);}
      sessDC.update(checkScroll(data));
      break;
    case '6':
      if(data.x.length<=7){postC.xAxis[0].setExtremes(0, data.x.length-1);}
      else{ postC.xAxis[0].setExtremes(0, 7);}
      postC.update(checkScroll(data));
      break;
    case '7':
      if(data.x.length<=7){likesC.xAxis[0].setExtremes(0, data.x.length-1);}
      else{ likesC.xAxis[0].setExtremes(0, 7);}
      likesC.update(checkScroll(data));
      break;
    case '8':
      if(data.x.length<=7){commentC.xAxis[0].setExtremes(0, data.x.length-1);}
      else{ commentC.xAxis[0].setExtremes(0, 7);}
      commentC.update(checkScroll(data));
      break;
    case '9':
      if(data.x.length<=7){likecsC.xAxis[0].setExtremes(0, data.x.length-1);}
      else{ likecsC.xAxis[0].setExtremes(0, 7);}
      likecsC.update(checkScroll(data));
      break;
    case '11':
      global_chart.update(checkScroll(data));
      break;
  }
}
function checkScroll(data){
  let result=null;
  if(data.x.length>7){
    console.log('>7');
    result= {
        xAxis: {
          categories: data.x,
          max: 7,
          scrollbar: {
            enabled: true,
            barBackgroundColor: "#dbdcde",
            rifleColor: false,
            barBorderRadius: 7,
            buttonBorderRadius: 7,
            buttonBackgroundColor: "#dbdcde"
          }
        },
        series: [{         
          data: data.y          
        }] 
    };
  }else{
    console.log('<7');
    result= {
        xAxis: {
          categories: data.x,
          max: data.x.length-1,
          scrollbar: {
            enabled: false            
          }
        },
        series: [{         
          data: data.y          
        }] 
    };
  }
  return result;
}
function uniqueUserChart(){
  let mychart={
  chart: {
    type: 'spline',    
  },
  title: {
    text: ''
  },
  credits: {
    enabled: false
  },
  yAxis: { 
    title: {
      text: ''
    }},
  xAxis: {
    categories: period,       
  }, 
  plotOptions: {
    series: {
      label: {       
        enabled: false,
        connectorAllowed: false
      }
    }
  },
  series: [{
    name: 'Utilisateur uniques',
    data: unicRetailers,
    color:"#b0cff4"   
  }],   
  }
  if(period.length>7){
    mychart.xAxis.max= 7;
    mychart.xAxis.scrollbar=scrollObj;
  }
 uniUserC=Highcharts.chart('unic-retailer-chart', mychart); 
}

function activeUserChart(){
  let mychart={

  chart: {
    type: 'areaspline',    
  },
  title: {
    text: ''
  },
  credits: {
    enabled: false
  },
  yAxis: { 
    title: {
      text: ''
    }},

  xAxis: {
    categories: period
  },
    

  plotOptions: {
    series: {
      label: {       
        enabled: false,
        connectorAllowed: false
      },
      fillOpacity: 0.6,
      marker: {
        enabled: false
      }
    }
  },

  series: [{
    name: 'Utilisateurs actifs',
    data: activeRetailers,
    color:"#5e56f4"   
  }] 

  };
  if(period.length>7){
    mychart.xAxis.max= 7;
    mychart.xAxis.scrollbar=scrollObj;
  }
 actiUserC=Highcharts.chart('active-retailer-chart', mychart); 
}
function commentsChart(){
  let mychart={
  chart: {
    type: 'column'    
  },
  title: {
    text: ''
  },
  credits: {
    enabled: false
  },
  yAxis: { 
    title: {
      text: ''
    }},
  xAxis: {
    categories: period
  },
  plotOptions: {
    series: {
      label: {       
        enabled: false,
        connectorAllowed: false
      },
      fillOpacity: 0.6,
      marker: {
        enabled: false
      }
    }
  },
  series: [{
    name: 'Commentaires',
    data: comments,
    color:"#FF9800"   
  }] 

  };
  if(period.length>7){
    mychart.xAxis.max= 7;
    mychart.xAxis.scrollbar=scrollObj;
  }
  commentC=Highcharts.chart('comments-chart', mychart); 
}
function likecsChart(){
  let mychart={
  chart: {
    type: 'column'    
  },
  title: {
    text: ''
  },
  credits: {
    enabled: false
  },
  yAxis: { 
    title: {
      text: ''
    }},
  xAxis: {
    categories: period
  },
  plotOptions: {
    series: {
      label: {       
        enabled: false,
        connectorAllowed: false
      },
      fillOpacity: 0.6,
      marker: {
        enabled: false
      }
    }
  },
  series: [{
    name: 'Like of Comments',
    data: likecs,
    color:"#f44336"   
  }] 

  };
  if(period.length>7){
    mychart.xAxis.max= 7;
    mychart.xAxis.scrollbar=scrollObj;
  }
  likecsC=Highcharts.chart('like-comments-chart', mychart); 
}
function postsChart(){
  let mychart={
  chart: {
    type: 'column'    
  },
  title: {
    text: ''
  },
  credits: {
    enabled: false
  },
  yAxis: { 
    title: {
      text: ''
    }},
  xAxis: {
    categories: period
  },
  plotOptions: {
    series: {
      label: {       
        enabled: false,
        connectorAllowed: false
      },
      fillOpacity: 0.6,
      marker: {
        enabled: false
      }
    }
  },
  series: [{
    name: 'publications',
    data: posts,
    color:"#fda7a4"   
  }] 

  };
  if(period.length>7){
    mychart.xAxis.max= 7;
    mychart.xAxis.scrollbar=scrollObj;
  }
  postC=Highcharts.chart('posts-chart', mychart); 
}
function likesChart(){
  let mychart={

  chart: {
    type: 'column'    
  },
  title: {
    text: ''
  },
  credits: {
    enabled: false
  },
  yAxis: { 
    title: {
      text: ''
    }},

  xAxis: {
    categories: period
  },
    

  plotOptions: {
    series: {
      label: {       
        enabled: false,
        connectorAllowed: false
      },
      fillOpacity: 0.6,
      marker: {
        enabled: false
      }
    }
  },

  series: [{
    name: 'likes',
    data: likes,
    color:"#6a6a6a"   
  }] 

  };
  if(period.length>7){
    mychart.xAxis.max= 7;
    mychart.xAxis.scrollbar=scrollObj;
  }
  likesC=Highcharts.chart('likes-chart', mychart); 
}
function sessionDurationChart(){
  let mychart={

  chart: {
    type: 'spline',    
  },
  title: {
    text: ''
  },
  credits: {
    enabled: false
  },
  yAxis: { 
    title: {
      text: ''
    }},

  xAxis: {
    categories: period
  },
    

  plotOptions: {
    series: {
      label: {       
        enabled: false,
        connectorAllowed: false
      }
    }
  },

  series: [{
    name: 'Session duration',
    data: durations,
    color:"#b0cff4"   
  }] 

  };
  if(period.length>7){
    mychart.xAxis.max= 7;
    mychart.xAxis.scrollbar=scrollObj;
    console.log('hu');
  }
  sessDC=Highcharts.chart('session-duration-chart', mychart); 
  console.log('sessDCD',sessDC);
}

function newRetailerChart(){
  let mychart= {

  chart: {
    type: 'bar',    
  },
  title: {
    text: ''
  },
  credits: {
    enabled: false
  },
  yAxis: { 
    title: {
      text: ''
    }},

  xAxis: {
    categories: period
  },
    

  plotOptions: {
    series: {
      label: {       
        enabled: false,
        connectorAllowed: false
      }
    }
  },

  series: [{
    name: 'Nouveaux Utilisateurs',
    data: newRetailers,
    color:"#f986c7"   
  }] 

  };
  if(period.length>7){
    mychart.xAxis.max= 7;
    mychart.xAxis.scrollbar=scrollObj;
  }
 newRC=Highcharts.chart('new-retailer-chart',mychart ); 
}
function sessionDurationDateChart(){
  let mychart={

  chart: {
    type: 'bar',    
  },
  title: {
    text: ''
  },
  credits: {
    enabled: false
  },
  yAxis: { 
    title: {
      text: ''
    }},

  xAxis: {
    categories: period
  },
    

  plotOptions: {
    series: {
      label: {       
        enabled: false,
        connectorAllowed: false
      }
    }
  },

  series: [{
    name: 'Nombre de sessions',
    data: sessionDurationsDate,
    color: {
        linearGradient: {
          x1: 0,
          x2: 0,
          y1: 0,
          y2: 1
        },
        stops: [
          [0, '#e4e6e8'],
          [1, '#6aad8d']
        ]
      }  
  }] 

  };
  if(period.length>7){
    mychart.xAxis.max= 7;
    mychart.xAxis.scrollbar=scrollObj;    
  }
 sessDCD=Highcharts.chart('session-duration-date', mychart);  
}
function pieRegion(){
  Highcharts.chart('pie-region', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: ''
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    credits: {
    enabled: false
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            }
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: regions
    }]
});
}
function nbPostByThme(){
  let mychart={
    chart: {
        type: 'bar'
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: themes_name
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Total posts'
        }
    },
    credits: {
      enabled: false
    },
    legend: {
        reversed: true
    },
    plotOptions: {
        series: {
            stacking: 'normal'
        }
    },
    series: nbPostByTheme
};
  
  Highcharts.chart('pie-nb-post-by-theme', mychart);


}

function globalChart(){
  let mychart={

  title: {
    text: ''
  },
  chart: {
    type: 'spline',
    
  }
  ,
  subtitle: {
    text: ''
  },
  credits: {
      enabled: false
    },
  yAxis: {
    title: {
      text: ''
    }
  },

  xAxis: {
    categories: period
  },
  legend: {
    layout: 'vertical',
    align: 'right',
    verticalAlign: 'top',
    symbolHeight: 12,
    symbolWidth: 12,
    symbolRadius: 6,
   // squareSymbol:true,
    symbolPadding: 20
  
  },   

  plotOptions: {
    series: {
      label: {
       
    enabled: false,

        connectorAllowed: false
      },
    marker: {
        enabled: false
      }
    }
  },

  series: [{
    name: 'Utilisateur uniques',    
    data: unicRetailers,
    color:"#b0cff4"
   
  }, {
    name: 'Utilisateur actifs',
    data: activeRetailers,
    color: "#887dff"
  }, {
    name: 'Nouveaux utilisateurs',
    data: newRetailers,
    color: "#f986c7"
    
  }, {
    name: 'Nombre de sessions',
    data: sessionDurationsDate,
    color: "#9cefc7"
  }, {
    name: 'Total likes',
    data: likes,
    color: "#7f7f7f"
  }, {
    name: 'Total posts',
    data: posts,
    color: "#fda7a4"
  }/*, {
    name: 'Durée moyenne des sessions',
    data: durations,
    color: "#8a4c64"
  }*/],

  

};
  if(period.length>7){
    mychart.xAxis.max= 7;
    mychart.xAxis.scrollbar=scrollObj;
  }
  global_chart=Highcharts.chart('global-chart', mychart);
}
