-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  ven. 15 mai 2020 à 13:15
-- Version du serveur :  10.1.32-MariaDB
-- Version de PHP :  7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `week_theme`
--

-- --------------------------------------------------------

--
-- Structure de la table `clients`
--

CREATE TABLE `clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `userid` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactid` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sessionid` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `touchpointids` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tradeprogram` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reasontype` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awardpoints` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reasondescription` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mailingcity` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobilephone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `origin` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `clients`
--

INSERT INTO `clients` (`id`, `userid`, `contactid`, `firstname`, `lastname`, `role`, `email`, `language`, `sessionid`, `touchpointids`, `tradeprogram`, `reasontype`, `awardpoints`, `reasondescription`, `mailingcity`, `mobilephone`, `origin`, `created_at`, `updated_at`) VALUES
(1, '1234', '141414', 'khalil', 'afif', NULL, '4141', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, '1441750', '414141', 'Souli ', 'Mohamed', NULL, 'pyh-yb', NULL, 'ybytb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '00558000004bZ6KAAU', '0035800001gQHhcAAG', 'Meher', 'Zargouni', 'Manager 1', 'meherzargouni@outlook.fr', 'fr', '00D58000000cA7m!ARsAQNywb2sqRtNq9EAuz.knDcHxUjC3v3Umr4O_CFzPcI9v2UNBr4cTLpCJDb417iI3FdNjWqRcm39u6Z9WtqtSpMm6swO4', '1', '00158000018l1xVAAQ', 'a0F5800000BBTbbEAH', '37881', 'Transaction for prize claim through Iframe Widget', 'Tunis', '26525600', 'SF', '2020-04-08 21:00:08', '2020-04-08 21:00:08'),
(4, 'fsbdfb', 'dvgsdf', 'Semah ', 'derbeli', NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 4, 'id', 'text', 'Id', 1, 1, 0, 0, 0, 0, '{}', 1),
(23, 4, 'date_begin', 'text', 'Date Begin', 1, 1, 1, 1, 1, 1, '{}', 2),
(24, 4, 'date_end', 'text', 'Date End', 1, 1, 1, 1, 1, 1, '{}', 3),
(25, 4, 'subject', 'text', 'Subject', 1, 1, 1, 1, 1, 1, '{}', 4),
(26, 4, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(27, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(28, 5, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(29, 5, 'state', 'text', 'State', 1, 1, 1, 1, 1, 1, '{}', 2),
(30, 5, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 3),
(31, 5, 'description', 'text', 'Description', 0, 1, 1, 1, 1, 1, '{}', 4),
(32, 5, 'media', 'text', 'Media', 0, 1, 1, 1, 1, 1, '{}', 5),
(33, 5, 'theme_id', 'text', 'Theme Id', 1, 1, 1, 1, 1, 1, '{}', 6),
(34, 5, 'client_id', 'text', 'Client Id', 1, 1, 1, 1, 1, 1, '{}', 7),
(35, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(36, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(37, 6, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(38, 6, 'client_id', 'text', 'Client Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(39, 6, 'post_id', 'text', 'Post Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(40, 6, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(41, 6, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(61, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(62, 8, 'userid', 'text', 'Userid', 1, 1, 1, 1, 1, 1, '{}', 2),
(63, 8, 'contactid', 'text', 'Contactid', 1, 1, 1, 1, 1, 1, '{}', 3),
(64, 8, 'firstname', 'text', 'Firstname', 1, 1, 1, 1, 1, 1, '{}', 4),
(65, 8, 'lastname', 'text', 'Lastname', 1, 1, 1, 1, 1, 1, '{}', 5),
(66, 8, 'role', 'text', 'Role', 0, 1, 1, 1, 1, 1, '{}', 6),
(67, 8, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{}', 7),
(68, 8, 'language', 'text', 'Language', 0, 1, 1, 1, 1, 1, '{}', 8),
(69, 8, 'sessionid', 'text', 'Sessionid', 1, 1, 1, 1, 1, 1, '{}', 9),
(70, 8, 'touchpointids', 'text', 'Touchpointids', 0, 1, 1, 1, 1, 1, '{}', 10),
(71, 8, 'tradeprogram', 'text', 'Tradeprogram', 0, 1, 1, 1, 1, 1, '{}', 11),
(72, 8, 'reasontype', 'text', 'Reasontype', 0, 1, 1, 1, 1, 1, '{}', 12),
(73, 8, 'awardpoints', 'text', 'Awardpoints', 0, 1, 1, 1, 1, 1, '{}', 13),
(74, 8, 'reasondescription', 'text', 'Reasondescription', 0, 1, 1, 1, 1, 1, '{}', 14),
(75, 8, 'mailingcity', 'text', 'Mailingcity', 0, 1, 1, 1, 1, 1, '{}', 15),
(76, 8, 'mobilephone', 'text', 'Mobilephone', 0, 1, 1, 1, 1, 1, '{}', 16),
(77, 8, 'origin', 'text', 'Origin', 0, 1, 1, 1, 1, 1, '{}', 17),
(78, 8, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 18),
(79, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 19),
(80, 4, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 2);

-- --------------------------------------------------------

--
-- Structure de la table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2020-04-02 16:02:34', '2020-04-02 16:02:34'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2020-04-02 16:02:34', '2020-04-02 16:02:34'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2020-04-02 16:02:34', '2020-04-02 16:02:34'),
(4, 'themes', 'themes', 'Theme', 'Themes', 'voyager-paint-bucket', 'App\\Theme', NULL, 'App\\Http\\Controllers\\ThemeController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-04-02 18:15:49', '2020-04-14 13:22:37'),
(5, 'posts', 'posts', 'Post', 'Posts', 'voyager-photos', 'App\\Post', NULL, 'App\\Http\\Controllers\\PostController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-04-02 18:16:34', '2020-04-14 13:26:49'),
(6, 'likes', 'likes', 'Like', 'Likes', 'voyager-thumbs-up', 'App\\Like', NULL, 'App\\Http\\Controllers\\LikeController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-04-02 18:17:35', '2020-04-14 13:28:31'),
(8, 'clients', 'clients', 'Client', 'Clients', 'voyager-people', 'App\\Client', NULL, 'App\\Http\\Controllers\\RetailerController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-04-14 13:18:39', '2020-04-14 18:10:57');

-- --------------------------------------------------------

--
-- Structure de la table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `likes`
--

CREATE TABLE `likes` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `likes`
--

INSERT INTO `likes` (`id`, `client_id`, `post_id`, `created_at`, `updated_at`) VALUES
(2, 3, 8, '2020-04-08 21:27:12', '2020-04-08 21:27:12'),
(3, 3, 9, '2020-04-10 11:33:43', '2020-04-10 11:33:43'),
(8, 3, 22, '2020-04-17 22:24:56', '2020-04-17 22:24:56'),
(9, 3, 24, '2020-04-17 22:24:58', '2020-04-17 22:24:58'),
(10, 3, 27, '2020-04-17 22:25:00', '2020-04-17 22:25:00'),
(16, 3, 28, '2020-04-20 11:41:43', '2020-04-20 11:41:43'),
(17, 4, 8, NULL, NULL),
(18, 2, 9, NULL, NULL),
(19, 1, 22, NULL, NULL),
(20, 4, 27, NULL, NULL),
(31, 3, 29, '2020-04-24 12:26:41', '2020-04-24 12:26:41');

-- --------------------------------------------------------

--
-- Structure de la table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-04-02 16:02:35', '2020-04-02 16:02:35');

-- --------------------------------------------------------

--
-- Structure de la table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2020-04-02 16:02:35', '2020-04-02 16:02:35', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2020-04-02 16:02:35', '2020-04-02 16:02:35', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2020-04-02 16:02:35', '2020-04-02 16:02:35', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2020-04-02 16:02:35', '2020-04-02 16:02:35', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 9, '2020-04-02 16:02:35', '2020-04-02 16:02:35', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 10, '2020-04-02 16:02:35', '2020-04-02 16:02:35', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 11, '2020-04-02 16:02:35', '2020-04-02 16:02:35', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 12, '2020-04-02 16:02:35', '2020-04-02 16:02:35', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 13, '2020-04-02 16:02:35', '2020-04-02 16:02:35', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 14, '2020-04-02 16:02:35', '2020-04-02 16:02:35', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 13, '2020-04-02 16:02:38', '2020-04-02 16:02:38', 'voyager.hooks', NULL),
(12, 1, 'Themes', '', '_self', NULL, NULL, NULL, 15, '2020-04-02 18:15:49', '2020-04-02 18:15:49', 'voyager.themes.index', NULL),
(13, 1, 'Posts', '', '_self', 'App\\Http\\Controllers\\PostController', NULL, NULL, 16, '2020-04-02 18:16:34', '2020-04-02 18:16:34', 'voyager.posts.index', NULL),
(14, 1, 'Likes', '', '_self', NULL, NULL, NULL, 17, '2020-04-02 18:17:35', '2020-04-02 18:17:35', 'voyager.likes.index', NULL),
(16, 1, 'Clients', '', '_self', NULL, NULL, NULL, 19, '2020-04-14 13:18:39', '2020-04-14 13:18:39', 'voyager.clients.index', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2020-04-02 16:02:36', '2020-04-02 16:02:36'),
(2, 'browse_bread', NULL, '2020-04-02 16:02:36', '2020-04-02 16:02:36'),
(3, 'browse_database', NULL, '2020-04-02 16:02:36', '2020-04-02 16:02:36'),
(4, 'browse_media', NULL, '2020-04-02 16:02:36', '2020-04-02 16:02:36'),
(5, 'browse_compass', NULL, '2020-04-02 16:02:36', '2020-04-02 16:02:36'),
(6, 'browse_menus', 'menus', '2020-04-02 16:02:36', '2020-04-02 16:02:36'),
(7, 'read_menus', 'menus', '2020-04-02 16:02:36', '2020-04-02 16:02:36'),
(8, 'edit_menus', 'menus', '2020-04-02 16:02:36', '2020-04-02 16:02:36'),
(9, 'add_menus', 'menus', '2020-04-02 16:02:36', '2020-04-02 16:02:36'),
(10, 'delete_menus', 'menus', '2020-04-02 16:02:36', '2020-04-02 16:02:36'),
(11, 'browse_roles', 'roles', '2020-04-02 16:02:36', '2020-04-02 16:02:36'),
(12, 'read_roles', 'roles', '2020-04-02 16:02:36', '2020-04-02 16:02:36'),
(13, 'edit_roles', 'roles', '2020-04-02 16:02:36', '2020-04-02 16:02:36'),
(14, 'add_roles', 'roles', '2020-04-02 16:02:36', '2020-04-02 16:02:36'),
(15, 'delete_roles', 'roles', '2020-04-02 16:02:36', '2020-04-02 16:02:36'),
(16, 'browse_users', 'users', '2020-04-02 16:02:36', '2020-04-02 16:02:36'),
(17, 'read_users', 'users', '2020-04-02 16:02:36', '2020-04-02 16:02:36'),
(18, 'edit_users', 'users', '2020-04-02 16:02:36', '2020-04-02 16:02:36'),
(19, 'add_users', 'users', '2020-04-02 16:02:36', '2020-04-02 16:02:36'),
(20, 'delete_users', 'users', '2020-04-02 16:02:36', '2020-04-02 16:02:36'),
(21, 'browse_settings', 'settings', '2020-04-02 16:02:36', '2020-04-02 16:02:36'),
(22, 'read_settings', 'settings', '2020-04-02 16:02:37', '2020-04-02 16:02:37'),
(23, 'edit_settings', 'settings', '2020-04-02 16:02:37', '2020-04-02 16:02:37'),
(24, 'add_settings', 'settings', '2020-04-02 16:02:37', '2020-04-02 16:02:37'),
(25, 'delete_settings', 'settings', '2020-04-02 16:02:37', '2020-04-02 16:02:37'),
(26, 'browse_hooks', NULL, '2020-04-02 16:02:38', '2020-04-02 16:02:38'),
(27, 'browse_themes', 'themes', '2020-04-02 18:15:49', '2020-04-02 18:15:49'),
(28, 'read_themes', 'themes', '2020-04-02 18:15:49', '2020-04-02 18:15:49'),
(29, 'edit_themes', 'themes', '2020-04-02 18:15:49', '2020-04-02 18:15:49'),
(30, 'add_themes', 'themes', '2020-04-02 18:15:49', '2020-04-02 18:15:49'),
(31, 'delete_themes', 'themes', '2020-04-02 18:15:49', '2020-04-02 18:15:49'),
(32, 'browse_posts', 'posts', '2020-04-02 18:16:34', '2020-04-02 18:16:34'),
(33, 'read_posts', 'posts', '2020-04-02 18:16:34', '2020-04-02 18:16:34'),
(34, 'edit_posts', 'posts', '2020-04-02 18:16:34', '2020-04-02 18:16:34'),
(35, 'add_posts', 'posts', '2020-04-02 18:16:34', '2020-04-02 18:16:34'),
(36, 'delete_posts', 'posts', '2020-04-02 18:16:34', '2020-04-02 18:16:34'),
(37, 'browse_likes', 'likes', '2020-04-02 18:17:35', '2020-04-02 18:17:35'),
(38, 'read_likes', 'likes', '2020-04-02 18:17:35', '2020-04-02 18:17:35'),
(39, 'edit_likes', 'likes', '2020-04-02 18:17:35', '2020-04-02 18:17:35'),
(40, 'add_likes', 'likes', '2020-04-02 18:17:35', '2020-04-02 18:17:35'),
(41, 'delete_likes', 'likes', '2020-04-02 18:17:35', '2020-04-02 18:17:35'),
(47, 'browse_clients', 'clients', '2020-04-14 13:18:39', '2020-04-14 13:18:39'),
(48, 'read_clients', 'clients', '2020-04-14 13:18:39', '2020-04-14 13:18:39'),
(49, 'edit_clients', 'clients', '2020-04-14 13:18:39', '2020-04-14 13:18:39'),
(50, 'add_clients', 'clients', '2020-04-14 13:18:39', '2020-04-14 13:18:39'),
(51, 'delete_clients', 'clients', '2020-04-14 13:18:39', '2020-04-14 13:18:39');

-- --------------------------------------------------------

--
-- Structure de la table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 3),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(27, 3),
(28, 1),
(28, 3),
(29, 1),
(29, 3),
(30, 1),
(30, 3),
(31, 1),
(31, 3),
(32, 1),
(32, 3),
(33, 1),
(33, 3),
(34, 1),
(34, 3),
(35, 1),
(36, 1),
(37, 1),
(37, 3),
(38, 1),
(38, 3),
(39, 1),
(40, 1),
(41, 1),
(47, 1),
(47, 3),
(48, 1),
(48, 3),
(49, 1),
(50, 1),
(51, 1);

-- --------------------------------------------------------

--
-- Structure de la table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `state` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `media` text COLLATE utf8mb4_unicode_ci,
  `theme_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `posts`
--

INSERT INTO `posts` (`id`, `state`, `title`, `description`, `media`, `theme_id`, `client_id`, `created_at`, `updated_at`) VALUES
(8, 'acceptee', 'Bonjour', 'yuppppppp', 'http://localhost/week-theme/public/uploaded/3_1586383869.png', 3, 3, '2020-04-08 21:11:09', '2020-04-08 21:11:09'),
(9, 'acceptee', 'Bonjour', 'yuppppppp', 'http://localhost/week-theme/public/uploaded/3_1586458147.png', 3, 3, '2020-04-09 17:49:07', '2020-04-09 17:49:07'),
(11, 'en attente', 'dcsdc', 'csdcdsc', 'http://localhost/week-theme/public/uploaded/3_1586515397.PNG', 3, 3, '2020-04-10 09:43:17', '2020-04-10 09:43:17'),
(12, 'en attente', 'sdcsdcsdc', 'dscsdcsdcsd', 'http://localhost/week-theme/public/uploaded/3_1586515768.png', 3, 3, '2020-04-10 09:49:28', '2020-04-10 09:49:28'),
(13, 'en attente', 'dfbdfbdfbdf', 'dfbdfb', 'http://localhost/week-theme/public/uploaded/3_1586516650.png', 3, 3, '2020-04-10 10:04:10', '2020-04-10 10:04:10'),
(14, 'en attente', 'dbffdb', 'fdbdfbdfb', 'http://localhost/week-theme/public/uploaded/3_1586516757.png', 3, 3, '2020-04-10 10:05:57', '2020-04-10 10:05:57'),
(15, 'en attente', 'fgnfgn', 'gfnfgnf', 'http://localhost/week-theme/public/uploaded/3_1586516789.jpg', 3, 3, '2020-04-10 10:06:29', '2020-04-10 10:06:29'),
(16, 'en attente', 'sdvsdv', 'sdvsdvsd', 'http://localhost/week-theme/public/uploaded/3_1586516843.jpg', 3, 3, '2020-04-10 10:07:23', '2020-04-10 10:07:23'),
(17, 'en attente', 'sdgsdsb', 'sdvbsdbsfb', 'http://localhost/week-theme/public/uploaded/3_1586518172.jpg', 3, 3, '2020-04-10 10:29:32', '2020-04-10 10:29:32'),
(18, 'en attente', 'thrth', 'rthrth', 'http://localhost/week-theme/public/uploaded/3_1586518291.jpg', 3, 3, '2020-04-10 10:31:31', '2020-04-10 10:31:31'),
(19, 'en attente', 'dvsdv', 'sdvdsvsdv', 'http://localhost/week-theme/public/uploaded/3_1586524620.PNG', 3, 3, '2020-04-10 12:17:00', '2020-04-10 12:17:00'),
(20, 'en attente', 'fgnfgn', 'fgnfgnfgn', 'http://localhost/week-theme/public/uploaded/3_1586524789.PNG', 3, 3, '2020-04-10 12:19:49', '2020-04-10 12:19:49'),
(21, 'en attente', 'efezf', 'ezfzefzef', 'http://localhost/week-theme/public/uploaded/3_1586524889.PNG', 3, 3, '2020-04-10 12:21:29', '2020-04-10 12:21:29'),
(22, 'acceptee', 'Saleeeeeeeeeeemmmm', 'Saaaleeeeeeeeeeeeeeemmmmmm', 'http://localhost/week-theme/public/uploaded/3_1587142634.jpg', 3, 3, '2020-04-17 15:57:14', '2020-04-17 15:58:31'),
(23, 'en attente', 'show show', 'fvfdv', 'http://localhost/week-theme/public/uploaded/3_1587143425.jpg', 3, 3, '2020-04-17 16:10:25', '2020-04-17 16:10:25'),
(24, 'acceptee', 'null', 'null', 'http://localhost/week-theme/public/uploaded/3_1587164667.png', 3, 3, '2020-04-17 22:04:27', '2020-04-17 22:05:20'),
(25, 'en attente', 'null', 'null', 'http://localhost/week-theme/public/uploaded/3_1587164811.png', 3, 3, '2020-04-17 22:06:51', '2020-04-17 22:06:51'),
(26, 'en attente', 'fdbdfbdfbdfbfd', 'null', 'http://localhost/week-theme/public/uploaded/3_1587164827.png', 3, 3, '2020-04-17 22:07:07', '2020-04-17 22:07:07'),
(27, 'acceptee', 'null', 'null', 'http://localhost/week-theme/public/uploaded/3_1587165076.jpg', 3, 3, '2020-04-17 22:11:16', '2020-04-17 22:20:23'),
(28, 'acceptee', 'gfngfngfn', 'fgngfnfgnSaaaleeeeeeeeeeeeeeemmmmmmSaaaleeeeeeeeeeeeeeemmmmmmSaaaleeeeeeeeeeeeeeemmmmmmSaaaleeeeeeeeeeeeeeemmmmmmSaaaleeeeeeeeeeeeeeemmmmmmSaaaleeeeeeeeeeeeeeemmmmmmSaaaleeeeeeeeeeeeeeemmmmmmSaaaleeeeeeeeeeeeeeemmmmmmSaaaleeeeeeeeeeeeeeemmmmmm', NULL, 3, 3, '2020-04-17 22:14:30', '2020-04-17 22:14:54'),
(29, 'acceptee', 'teesssssssssssssss', 'null', NULL, 4, 3, '2020-04-20 13:47:45', '2020-04-23 13:22:41'),
(35, 'en attente', 'szvfdbfdbdfbdf', 'null', NULL, 4, 3, '2020-04-20 13:49:00', '2020-04-20 13:49:00'),
(36, 'en attente', 'dfbdbdbdfb', 'null', NULL, 4, 3, '2020-04-20 13:49:08', '2020-04-20 13:49:08'),
(37, 'en attente', 'd sddv fd', 'null', NULL, 4, 3, '2020-04-20 13:50:22', '2020-04-20 13:50:22'),
(38, 'en attente', 'dscdscdsc', 'null', NULL, 4, 3, '2020-04-20 13:50:29', '2020-04-20 13:50:29'),
(39, 'en attente', 'fbdfb', 'dfbfdbfdb', 'http://localhost/week-theme/public/uploaded/3_1587651711.png', 4, 3, '2020-04-23 13:21:51', '2020-04-23 13:21:51');

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2020-04-02 16:02:35', '2020-04-02 16:02:35'),
(2, 'user', 'Normal User', '2020-04-02 16:02:35', '2020-04-02 16:02:35'),
(3, 'Cm', 'Cm', '2020-04-07 11:39:21', '2020-04-07 11:39:21');

-- --------------------------------------------------------

--
-- Structure de la table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Theme Of The Week', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', 'settings\\April2020\\a0kvvFxgXOzdL0TIfY4h.png', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Structure de la table `themes`
--

CREATE TABLE `themes` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_begin` date NOT NULL,
  `date_end` date NOT NULL,
  `subject` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `themes`
--

INSERT INTO `themes` (`id`, `title`, `date_begin`, `date_end`, `subject`, `created_at`, `updated_at`) VALUES
(3, 'ﺍﻟﺠﻤﻌﺔ الثانية', '2020-04-20', '2020-04-26', 'Hello', '2020-04-09 12:23:06', '2020-04-09 12:23:06'),
(4, 'test', '2020-04-27', '2020-05-03', 'حكيلنا و لا ورينا احنا والدخاخنية الي كيفك كيفاه تعدي في وقتك! الي مازال يخدم يبعث تصويرة مل حانوت! و يلا لمو الجامات! تصويرتك/تعليقك يهبط في ظرف 24 ساعة', '2020-04-20 11:14:08', '2020-04-20 13:38:35');

-- --------------------------------------------------------

--
-- Structure de la table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'admin@admin.com', 'users/default.png', NULL, '$2y$10$ngx.d2WwxH0EreVTLmsVGOmypO6qRjg/VVUGDS.nQ4o50XH4KgEBG', NULL, NULL, '2020-04-02 16:05:19', '2020-04-02 16:05:19'),
(2, 3, 'CM PMA', 'pma@themeweek.com', 'users\\April2020\\WZYiZ1ARpCW351ipuer1.png', NULL, '$2y$10$CEDoJ720D0jBcZA0hDXbtuc70wpkQa3vFt0wg5G7utyiO0I9BX1W6', NULL, '{\"locale\":\"en\"}', '2020-04-07 11:40:57', '2020-04-07 11:40:57');

-- --------------------------------------------------------

--
-- Structure de la table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user_roles`
--

INSERT INTO `user_roles` (`user_id`, `role_id`) VALUES
(2, 3);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`),
  ADD KEY `userid_2` (`userid`);

--
-- Index pour la table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Index pour la table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Index pour la table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Index pour la table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Index pour la table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Index pour la table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Index pour la table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Index pour la table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `state` (`state`);

--
-- Index pour la table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Index pour la table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Index pour la table `themes`
--
ALTER TABLE `themes`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Index pour la table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT pour la table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT pour la table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT pour la table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT pour la table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT pour la table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT pour la table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `themes`
--
ALTER TABLE `themes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Contraintes pour la table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
