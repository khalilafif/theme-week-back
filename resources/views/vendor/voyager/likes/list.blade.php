@extends('voyager::master')
@section('head')
   
@endsection
@section('page_header')
    <h1 class="page-title">
         likes
    </h1>
@stop

@section('content')

    <div class="page-content container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        @if($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                        @endif
                        <table id="dataTable" class="table table-hover">
                            <thead>
                            <tr>   
                                <th>Like numéro</th>  
                                <th>Retailer</th>                                                           
                                <th>Date</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($likes as $like)
                                    <tr>   
                                        <td>                                       
                                            {{$loop->iteration}}
                                        </td>  
                                        <td>                                       
                                            {{$like->client->firstname}} {{$like->client->lastname}}
                                        </td>                               
                                        <td>                                       
                                            {{$like->created_at}}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@stop

@section('javascript')
    
    <!-- DataTables -->
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable({
                "order": [],
                "language": {!! json_encode(__('voyager::datatable'), true) !!},
                "columnDefs": [{"targets": -1, "searchable":  false, "orderable": false}]
                @if(config('dashboard.data_tables.responsive')), responsive: true @endif
            });
        });
    </script>
@stop