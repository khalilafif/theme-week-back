@extends('voyager::master')
@section('head')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
    
@endsection
@section('page_header')
    <h1 class="page-title">
         Retailers
    </h1>
@stop

@section('content')

    <div class="page-content container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        @if($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                        @endif
        <table id="example" class="display nowrap">
        <thead>
            <tr>
                <th>Nom</th>
                <th>Prénom</th>
                <th>Comments</th>
                <th>Com accepté</th>
                <th>Com refusé</th>
                <th>Com supprimé</th>
                <th>Posts</th>
                <th>Post accepté</th>
                <th>Post refusé</th>
                <th>Post supprimé</th>
                <th>Likes collecté</th>
                <th>Likes effectué</th>
                <th>Contact id</th>
            </tr>
        </thead>
        <tbody>
            @foreach($clients as $client)
            <tr> 
                <td>{{$client->firstname}}</td>
                <td> {{$client->lastname}}</td>
                <td><a href="{{route('retailer-comment',$client->id)}}">{{$client->comments->count()}}</a> </td>
                <td><a href="{{route('filter-retailer',['model'=>2,'type'=>2,'id'=>$client->id])}}">{{$client->c_acceptee}}</a></td>
                <td><a href="{{route('filter-retailer',['model'=>2,'type'=>1,'id'=>$client->id])}}">{{$client->c_refusee}}</a></td>
                <td><a href="{{route('filter-retailer',['model'=>2,'type'=>3,'id'=>$client->id])}}">{{$client->c_deleted}}</a></td>
                <td><a href="{{route('list-posts-per-retailer',$client->id)}}">{{count($client->posts)}}</a> </td>
                <td><a href="{{route('filter-retailer',['model'=>1,'type'=>2,'id'=>$client->id])}}">{{$client->p_acceptee}}</a></td>
                <td><a href="{{route('filter-retailer',['model'=>1,'type'=>1,'id'=>$client->id])}}">{{$client->p_refusee}}</a></td>
                <td><a href="{{route('filter-retailer',['model'=>1,'type'=>3,'id'=>$client->id])}}">{{$client->p_deleted}}</a></td>
                <td>{{$client->like_collected}}</a></td>
                <td>{{count($client->likes)}}</td>
                <td>{{$client->contactid}}</td>
            </tr>
            @endforeach
        </tbody>
        </table>
       
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    
    
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
    <script>
    $(document).ready(function() {
        $('#example').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                {extend: 'excel',
                text: 'Export excel',
                className: 'btn btn-primary'}
            ],
            order: [],
            language: {!! json_encode(__('voyager::datatable'), true) !!},
            columnDefs: [{"targets": -1, "searchable":  false, "orderable": false}]
                @if(config('dashboard.data_tables.responsive')), responsive: true @endif
        } );
    } );
    </script>
@stop
