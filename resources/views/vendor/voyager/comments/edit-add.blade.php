@extends('voyager::master')
@section('head')
    <style>
        h5{
            font-weight: bold;;
        }
        h3{
            color: #22a7f0;
        }        
    </style>
@endsection
@section('content')

    <h1 class="page-title">
        <i class=""></i>
        Update Comment
    </h1>
    <div id="voyager-notifications"></div>
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                        <div class="panel-body"> 
                            @if($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif 
                            <div class="row">
                            <div class="col-md-6">     
                            <div class="form-group  col-md-12">
                                <h5>Titre</h5>
                                <p>{{$comment->post->title}}</p>
                            </div>
                            <div class="form-group  col-md-12 ">
                                <h5>Déscription</h5>
                                <p>{{$comment->post->description}}</p>
                            </div>
                            <div class="form-group  col-md-12 ">
                                <h5>Image</h5>
                                <img src="{{$comment->post->media}}" style="max-width:50%;">
                            </div>
                            </div>
                            <div class="form-group  col-md-6 ">
                                <h3>Commentaire</h3>
                                <div class="form-group  col-md-12 ">
                                    <h5>Contenu</h5>
                                    <p>{{$comment->content}}</p>
                                </div>
                                 <!-- form start -->
                                <form role="form" action="{{route('validation-comment')}}" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="comment_id" value="{{$comment->id}}">
                                    <!-- CSRF TOKEN -->
                                     @csrf
                                <div class="form-group  col-md-12 ">
                                <h5>Etat</h5>
                                <select name="state" id="state" class="form-control"> 
                                    <option value="en attente" @if($comment->state=="en attente")selected @endif>en attente</option>
                                    <option value="acceptee" @if($comment->state=="acceptee")selected @endif>acceptee</option>
                                    <option value="refusee" @if($comment->state=="refusee")selected @endif>refusee</option>
                                </select>
                                </div>
                                <div class="form-group bloc-reason @if($comment->state!='refusee')hidden @endif col-md-12">
                                <h5 style="margin-top: 15px;">Raison</h5>
                                <select name="reason" id="reason" class="form-control">
                                    <option value="">
                                        Choisir
                                    </option>

                                    <option value="التعليق ما تقبلش خاتر خارج موضوع الجمعة هاذي"
                                     @if($comment->reason=="التعليق ما تقبلش خاتر خارج موضوع الجمعة هاذي")selected @endif>
                                     التعليق ما تقبلش خاتر خارج موضوع الجمعة هاذي
                                    </option>

                                    <option value="التعليق ما تقبلش خاترفما صغيرات فيه"
                                     @if($comment->reason=="التعليق ما تقبلش خاترفما صغيرات فيه")selected @endif>
                                     التعليق ما تقبلش خاترفما صغيرات فيه
                                    </option>

                                    <option value="التعليق ما تقبلش خاتر ما يوافقش تعاليم وشروط الموقع"
                                     @if($comment->reason=="التعليق ما تقبلش خاتر ما يوافقش تعاليم وشروط الموقع")selected @endif>
                                        التعليق ما تقبلش خاتر ما يوافقش تعاليم وشروط الموقع
                                    </option>

                                    <option value="التعليق ما تقبلش خاتروقع نشره سابقا"
                                     @if($comment->reason=="التعليق ما تقبلش خاتروقع نشره سابقا")selected @endif>
                                     التعليق ما تقبلش خاتروقع نشره سابقا
                                    </option>                                    
                                </select>
                                <br>
                                </div>
                                <button type="submit" class="btn btn-primary save pull-right">Save</button>
                                </form>
                            </div>
                        </div>  
                        </div><!-- panel-body -->
                        <div class="panel-footer">
                        </div>
                </div>
            </div>
        </div>
    </div>

    
@endsection

@section('javascript')
<script type="text/javascript">
    $('#state').on('change',function(){
        if(this.value=="refusee"){
            $('.bloc-reason').removeClass('hidden');
            $('#reason').attr('required',true)
        }else{
            $('.bloc-reason').addClass('hidden');
            $('#reason').attr('required',false)
        }
        console.log('jhf',this.value);
    })
</script>
@endsection
