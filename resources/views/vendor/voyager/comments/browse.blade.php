@extends('voyager::master')
@section('head')
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
    <style>
        #example_filter, #example_paginate,#example_info{
            display:none;
        }
    </style>
@endsection
@section('page_header')
    <h1 class="page-title">
        Commentaires
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="row">         
                        <hr>
                        @if($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                        @endif
                        <h3>Liste des commentaires en attente</h3> 
                        <table id="dataTable" class="table table-hover">
                            <thead>
                            <tr>   
                                <th>comment numéro</th>  
                                <th>Retailer</th> 
                                <th>Etat</th>
                                <th>Contenu</th>                           
                                <th>Date</th>
                                <th>Time</th>
                                <th>Update_at</th>         
                                <th class="actions text-right">{{ __('voyager::generic.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($waitingCom as $comment)
                                    <tr>   
                                        <td>{{$comment->id}}</td>  
                                        <td><a href="{{route('client-info',$comment->client->id)}}">{{$comment->client->firstname}} {{$comment->client->lastname}}</a></td>
                                        <td>{{$comment->state}}</td>
                                        <td>{{$comment->content}}</td>
                                        <td>       
                                             {{date("Y-m-d",strtotime(date($comment->created_at)))}}
                                        </td>
                                        <td>       
                                             {{date("H:m:s",strtotime(date($comment->created_at)))}}     
                                        </td>
                                        <td> {{$comment->updated_at}}         
                                        </td>
                                        <td class="no-sort no-click bread-actions">
                                            <a href="{{route('edit-comment',$comment->id)}}" class="btn btn-sm btn-primary pull-right edit-reco" >
                                                <i class="voyager-edit"></i> {{ __('voyager::generic.edit') }}
                                            </a>                         
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <br><br>
                        <hr>
                        <br><br>
                        <h3>Liste des commentaires valide/réfusé</h3> 
                        <table id="dataTable1" class="table table-hover">
                            <thead>
                            <tr>   
                                <th>comment numéro</th>  
                                <th>Retailer</th> 
                                <th>Etat</th>
                                <th>Contenu</th>
                                <th>Likes</th>           
                                <th>Date</th>
                                <th>Time</th>
                                <th>Update_at</th>         
                                <th class="actions text-right">{{ __('voyager::generic.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($otherCom as $comment)
                                    <tr>   
                                        <td>{{$comment->id}}</td>  
                                        <td><a href="{{route('client-info',$comment->client->id)}}">{{$comment->client->firstname}} {{$comment->client->lastname}}</a></td>
                                        <td>{{$comment->state}}</td>
                                        <td>{{$comment->content}}</td>
                                        <td>
                                            <a href="{{route('likes-comment',$comment->id)}}">{{$comment->likecs_count}}</a>
                                        </td>
                                        <td>       
                                             {{date("Y-m-d",strtotime(date($comment->created_at)))}}
                                        </td>
                                        <td>       
                                             {{date("H:m:s",strtotime(date($comment->created_at)))}}     
                                        </td>
                                        <td> {{$comment->updated_at}}         
                                        </td>
                                        <td class="no-sort no-click bread-actions">
                                            @if($comment->state=='refusee' || $comment->is_deleted)
                                            <button style="margin-left: 3px;" type="button" class="btn btn-sm btn-danger pull-right delete-obj" data-target="#exampleModal" data-toggle="modal" data-url="{{route('delete-cp',['type'=>2,'id'=>$comment->id ])}}">
                                                <i class="voyager-trash"></i> {{ __('voyager::generic.delete') }}
                                            </button>
                                            @endif   
                                            <a href="{{route('edit-comment',$comment->id)}}" class="btn btn-sm btn-primary pull-right edit-reco" >
                                                <i class="voyager-edit"></i> {{ __('voyager::generic.edit') }}
                                            </a>              
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-danger fade" tabindex="-1" role="dialog" id="exampleModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} Commentaire?
                    </h4>
                </div>
                <div class="modal-footer">        
                    <a href="" class="btn btn-danger pull-right" id="confirm-delete">Yes, Delete This </a>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>    
    <!-- DataTables -->
    <script>           
        $(document).ready(function () {
           var dataT= $('#dataTable').DataTable({
                "order": [],
                "language": {!! json_encode(__('voyager::datatable'), true) !!},
                "columnDefs": [{"targets": -1, "searchable":  false, "orderable": false}],
                "dom": 'Bfrtip',
                "buttons": [
                    {extend: 'excel',
                    text: 'Export excel',
                    className: 'btn btn-primary'}
                ]
                @if(config('dashboard.data_tables.responsive')), responsive: true @endif
            });
           var dataT= $('#dataTable1').DataTable({
                "order": [],
                "language": {!! json_encode(__('voyager::datatable'), true) !!},
                "columnDefs": [{"targets": -1, "searchable":  false, "orderable": false}],
                "dom": 'Bfrtip',
                "buttons": [
                    {extend: 'excel',
                    text: 'Export excel',
                    className: 'btn btn-success'}
                ]
                @if(config('dashboard.data_tables.responsive')), responsive: true @endif
            });
        });        
        $('.delete-obj').click(function (e) {
            let url= $(this).data('url');
            $('#confirm-delete').attr('href',url);
        });
    </script>
@stop