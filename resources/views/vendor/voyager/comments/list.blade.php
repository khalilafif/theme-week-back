@extends('voyager::master')
@section('head')
   
@endsection
@section('page_header')
    <h1 class="page-title">
         Commentaires
    </h1>
@stop

@section('content')

    <div class="page-content container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        @if($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                        @endif
                        <table id="dataTable" class="table table-hover">
                            <thead>
                            <tr>   
                                <th>Comment numéro</th>  
                                <th>Retailer</th>                 
                                <th>Etat</th>                 
                                <th>Content</th>
                                <th>Likes</th>
                                <th>Date</th>
                                <th>Supprimé par le retailer</th>
                                <th class="actions text-right">{{ __('voyager::generic.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($comments as $comment)
                                    <tr>   
                                        <td>      
                                            {{$loop->iteration}}
                                        </td>  
                                        <td>      
                                            {{$comment->client->firstname}} {{$comment->client->lastname}}
                                        </td>
                                        <td>      
                                            {{$comment->state}}
                                        </td>
                                        <td>      
                                            {{$comment->content}}
                                        </td>
                                        <td>     
                                            <a href="{{route('likes-comment',$comment->id)}}">{{$comment->likecs_count}}</a>
                                        </td>                         
                                        <td>     
                                            {{$comment->created_at}}
                                        </td>
                                        <td>
                                            @if($comment->is_deleted) Oui
                                            @else Non
                                            @endif    
                                        </td>
                                        <td class="no-sort no-click bread-actions">
                                            @if($comment->state=='refusee' || $comment->is_deleted)
                                            <button style="margin-left: 3px;" type="button" class="btn btn-sm btn-danger pull-right delete-obj" data-target="#exampleModal" data-toggle="modal" data-url="{{route('delete-cp',['type'=>2,'id'=>$comment->id ])}}">
                                                <i class="voyager-trash"></i> {{ __('voyager::generic.delete') }}
                                            </button>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-danger fade" tabindex="-1" role="dialog" id="exampleModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} Commentaire?
                    </h4>
                </div>
                <div class="modal-footer">        
                    <a href="" class="btn btn-danger pull-right" id="confirm-delete">Yes, Delete This </a>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    
    <!-- DataTables -->
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable({
                "order": [],
                "language": {!! json_encode(__('voyager::datatable'), true) !!},
                "columnDefs": [{"targets": -1, "searchable":  false, "orderable": false}]
                @if(config('dashboard.data_tables.responsive')), responsive: true @endif
            });
            $('.delete-obj').click(function (e) {
                let url= $(this).data('url');               
                $('#confirm-delete').attr('href',url);
            });
        });
    </script>
@stop
