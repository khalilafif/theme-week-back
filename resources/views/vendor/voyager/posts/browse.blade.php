@extends('voyager::master')
@section('head')
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
    <style>
        #example_filter, #example_paginate,#example_info{
            display:none;
        }
    </style>
@endsection
@section('page_header')
    <h1 class="page-title">
         Posts
    </h1>
@stop

@section('content')

    <div class="page-content container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-8">
                                <h4>Posts filtre</h4><br>
                                <div class="col-md-6">                                    
                                    <span class="metric-title" style="color: #e01763;margin-top: 5px;">Statut du Post</span>
                                    <select name="state" id="post-state" class="form-control"> 
                                        <option value="">Choisir</option>
                                        <option value="en attente">en attente</option>
                                        <option value="acceptee">acceptee</option>
                                        <option value="refusee">refusee</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    @if(!isset($withTheme))
                                        <span class="metric-title" style="color: #e01763;margin-top: 5px;">Théme</span>
                                        <select class="theme-filter form-control">
                                            <option value="">Choisir:</option>
                                        @foreach($themes as $theme)
                                            <option value="{{$theme->title}}">{{$theme->title}}</option>
                                        @endforeach
                                        </select>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <span class="metric-title" style="color: #e01763;margin-top: 15px;">Date début</span>
                                    <input type="date" id="date-debut" name="date" class="form-control date-range-filter">
                                </div>
                                <div class="col-md-6">
                                    <span class="metric-title" style="color: #e01763;margin-top: 15px;">Date fin</span>
                                    <input type="date" id="date-fin" name="date" class="form-control date-range-filter">
                                </div>
                            </div>                           
                            <div class="col-md-4 top-post-bloc"> 
                                <h3>Le post le plus liké</h3> 
                                <!--<select class=" theme-top-post">
                                    @foreach($themes as $theme)
                                        <option value="{{$theme->id}}" @if($loop->first)selected @endif>{{$theme->title}}</option>
                                    @endforeach
                                </select>-->
                                <h4 style="text-align: right;" class="hidden top-post-empty">Ce thème ne contient pas de posts pendant cette période</h4>
                                <br> 
                                <div class="bloc-top-post">
                                    @if(Auth::user()->role_id != 4)
                                        <button id="share-post" class="btn btn-primary @if($topPost->share)shared @endif" data-id="{{$topPost->id}}">
                                            <span class="icon voyager-forward"></span>
                                            <span class="sha-txt">@if($topPost->share)Shared @else Share @endif</span>
                                        </button>
                                    @endif
                                    <p id="post-title">@if(!is_null($topPost->title)){{$topPost->title}} <span class="nb-likes">Likes({{$topPost->likes_count}})</span>@endif</p>
                                    <p id="post-description">@if(!is_null($topPost->description)){{$topPost->description}}@endif</p>
                                    <img id="post-media" @if(is_null($topPost->media)) class="hidden" @endif src="{{$topPost->media}}" >
                                </div> 
                            
                        </div>
                        <hr>
                        @if($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                        @endif
                        <table id="dataTable" class="table table-hover">
                            <thead>
                            <tr>   
                                <th>Post numéro</th>  
                                <th>Retailer</th>                                                           
                                <th>Théme</th>
                                <th>Etat</th>
                                <th>Titre</th>   
                                <th>Likes</th> 
                                <th>Comments</th>           
                                <th>Date</th>
                                <th>Time</th>
                                <th>BO</th>  
                                <th>Update_at</th>                            
                                <th class="actions text-right">{{ __('voyager::generic.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($posts as $post)

                                    <tr>   
                                        <td>                                       
                                            {{$post->id}}
                                        </td>  
                                        <td><a href="{{route('client-info',$post->client->id)}}">{{$post->client->firstname}} {{$post->client->lastname}}</a></td>                               
                                        <td>                                       
                                            <a href="{{route('edit-theme',$post->theme->id)}}">{{$post->theme->title}}</a>
                                        </td>
                                        <td>       
                                            {{$post->state}}                            
                                        </td>
                                        <td>       
                                            {{$post->title}}                            
                                        </td>
                                        <td>       
                                            <a href="{{route('list-like',$post->id)}}">{{$post->likes_count}}</a>
                                        </td>
                                        <td>
                                             <a href="{{route('list-comment',$post->id)}}">
                                            {{$post->comments_count}}
                                             </a>

                                        </td>
                                        <td>       
                                             {{date("Y-m-d",strtotime(date($post->created_at)))}}                                  
                                        </td>
                                        <td>       
                                             {{date("H:m:s",strtotime(date($post->created_at)))}}                                  
                                        </td>
                                        <td>
                                            @if($post->user)
                                             {{$post->user->name}}                                  
                                            @endif
                                        </td>
                                        <td>       
                                             {{$post->updated_at}}                                  
                                        </td>
                                        <td class="no-sort no-click bread-actions">
                                            @if($post->state=='refusee' || $post->is_deleted)
                                            <button style="margin-left: 3px;" type="button" class="btn btn-sm btn-danger pull-right delete-obj" data-target="#exampleModal" data-toggle="modal" data-url="{{route('delete-cp',['type'=>1,'id'=>$post->id ])}}">
                                                <i class="voyager-trash"></i> {{ __('voyager::generic.delete') }}
                                            </button>
                                            @endif
                                            <a href="{{route('edit-post',$post->id)}}" class="btn btn-sm btn-primary pull-right edit-reco" >
                                                <i class="voyager-edit"></i> {{ __('voyager::generic.edit') }}
                                            </a>                              
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-danger fade" tabindex="-1" role="dialog" id="exampleModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} Commentaire?
                    </h4>
                </div>
                <div class="modal-footer">        
                    <a href="" class="btn btn-danger pull-right" id="confirm-delete">Yes, Delete This </a>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
    
    <!-- DataTables -->
    <script>
            var routeTopPost="{{route('get-top-post')}}";
            var routeSharePost="{{route('shared-post',0)}}";
            var token= "{!! csrf_token() !!}";
            var themes_id={!!json_encode($themes->pluck('id','title')->toArray())!!}
        $(document).ready(function () {
            $.fn.dataTable.ext.search.push(
              function(settings, data, dataIndex) {
                var datebegin = $('#date-debut').val()/*.split("-").reverse().join("/")*/;
                var dateend = $('#date-fin').val()/*.split("-").reverse().join("/")*/;
                var datede = data[6] || 0; // Our date column in the table
                if ((datebegin == "" || dateend == "") ||
                   (new Date(datede)>= new Date(datebegin) && new Date(datede) <= new Date(dateend))
                 ) {
                  return true;
                }

                return false;                
              }
            );

           var dataT= $('#dataTable').DataTable({
                "order": [],
                "language": {!! json_encode(__('voyager::datatable'), true) !!},
                "columnDefs": [{"targets": -1, "searchable":  false, "orderable": false}],
                "dom": 'Bfrtip',
                "buttons": [
                    {extend: 'excel',
                    text: 'Export excel',
                    className: 'btn btn-primary'}
                ]
                @if(config('dashboard.data_tables.responsive')), responsive: true @endif
            });

            $('#post-state').change(function(e){
                e.preventDefault();
                var value=$(this).val();
                if(value!=""){
                    var ch="";
                    for (var i = 0; i < value.length; i++) {
                        ch=ch+value[i];
                        dataT.column(3).search( ch ).draw();
                        console.log(ch);
                    }
                }else{
                    dataT.column(3).search("").draw();
                }
            });
            $('.theme-filter').change(function(e){
                e.preventDefault();
                var value=$(this).val();
                if(value!=""){
                    var ch="";
                    for (var i = 0; i < value.length; i++) {
                        ch=ch+value[i];
                        dataT.column(2).search( ch ).draw();
                        console.log(ch);
                    }
                }else{
                    dataT.column(2).search("").draw();
                }
            });
        // Re-draw the table when the a date range filter changes
            $('.date-range-filter').change(function() {
              dataT.draw();
            });

            $('.delete-obj').click(function (e) {
                let url= $(this).data('url');
                $('#confirm-delete').attr('href',url);
            });
        });        

    </script>
    <script type="text/javascript" src="{{asset('reporting/reporting.js?v=12')}}"></script>
@stop
