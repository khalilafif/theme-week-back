@extends('voyager::master')
@section('content')

    <h1 class="page-title">
        <i class=""></i>
        Update post
    </h1>
    <div id="voyager-notifications"></div>
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form" action="{{route('update-post')}}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="post_id" value="{{$post->id}}">
                        <!-- CSRF TOKEN -->
                         @csrf

                        <div class="panel-body"> 
                            @if($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif 
                            <div class="row">
                            <div class="col-md-6">                                                       
                            <div class="form-group  col-md-12">                                    
                                <h5>Titre</h5>
                                <p>{{$post->title}}</p>
                            </div>
                            <div class="form-group  col-md-12 ">                                    
                                <h5>Déscription</h5>
                                <p>{{$post->description}}</p>
                            </div>
                            <div class="form-group  col-md-12 ">                                    
                                <h5>Image</h5>
                                <img src="{{$post->media}}" style="max-width:70%;">
                            </div>
                            </div>
                            <div class="form-group  col-md-6 ">                                    
                                <h5>Etat</h5>
                                <select name="state" id="state" class="form-control"> 
                                    <option value="en attente" @if($post->state=="en attente")selected @endif>en attente</option>
                                    <option value="acceptee" @if($post->state=="acceptee")selected @endif>acceptee</option>
                                    <option value="refusee" @if($post->state=="refusee")selected @endif>refusee</option>
                                </select>
                                <div class="bloc-reason @if($post->state!='refusee')hidden @endif">
                                <h5 style="margin-top: 15px;">Raison</h5>
                                <select name="reason" id="reason" class="form-control">
                                    <option value="">
                                        Choisir
                                    </option>

                                    <option value="تصويرتك/نصك ما تقبلش خاتر خارج موضوع الجمعة هاذي"
                                     @if($post->reason=="تصويرتك/نصك ما تقبلش خاتر خارج موضوع الجمعة هاذي")selected @endif>
                                     تصويرتك/نصك ما تقبلش خاتر خارج موضوع الجمعة هاذي
                                    </option>

                                    <option value="تصويرتك/نصك ما تقبلش خاترفما صغيرات فيه"
                                     @if($post->reason=="تصويرتك/نصك ما تقبلش خاترفما صغيرات فيه")selected @endif>
                                     تصويرتك/نصك ما تقبلش خاترفما صغيرات فيه
                                    </option>

                                    <option value="تصويرتك/نصك ما تقبلش خاتر ما يوافقش تعاليم وشروط الموقع"
                                     @if($post->reason=="تصويرتك/نصك ما تقبلش خاتر ما يوافقش تعاليم وشروط الموقع")selected @endif>
                                     تصويرتك/نصك ما تقبلش خاتر ما يوافقش تعاليم وشروط الموقع
                                    </option>

                                    <option value="تصويرتك/نصك ما تقبلش خاتروقع نشره سابقا"
                                     @if($post->reason=="تصويرتك/نصك ما تقبلش خاتروقع نشره سابقا")selected @endif>
                                     تصويرتك/نصك ما تقبلش خاتروقع نشره سابقا
                                    </option>                                    
                                </select>
                                </div>
                                <button type="submit" class="btn btn-primary save ">Save</button>
                            </div>
                        </div>  
                        </div><!-- panel-body -->

                        <div class="panel-footer">
                           
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    
@endsection

@section('javascript')
<script type="text/javascript">
    $('#state').on('change',function(){
        if(this.value=="refusee"){
            $('.bloc-reason').removeClass('hidden');
            $('#reason').attr('required',true)
        }else{
            $('.bloc-reason').addClass('hidden');
            $('#reason').attr('required',false)
        }
        console.log('jhf',this.value);
    })
</script>
@endsection