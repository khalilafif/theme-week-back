@extends('voyager::master')
@section('content')

    <h1 class="page-title">
        <i class=""></i>
        Add theme
    </h1>
    <div id="voyager-notifications"></div>
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form" action=" @if(isset($theme)) {{route('update-theme')}} @else {{route('store-theme')}} @endif" method="POST" enctype="multipart/form-data">
                        @if(isset($theme))

                            <input type="hidden" name="theme_id" value="{{$theme->id}}">
                        @endif
                        <!-- CSRF TOKEN -->
                         @csrf

                        <div class="panel-body"> 
                            @if($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif  
                            <div class="form-group  col-md-12">                                    
                                <label class="control-label" for="name">Titre</label>
                                <input type="text" class="form-control" name="title" @if(isset($theme)) value="{{$theme->title}}" @endif required>                                   
                            </div>                                                        
                            <div class="form-group  col-md-12">                                    
                                <label class="control-label" for="name">Date début</label>
                                <input type="date" class="form-control" name="datedebut" @if(isset($theme)) value="{{$theme->date_begin}}" @endif required>                                   
                            </div>
                            <div class="form-group  col-md-12 ">                                    
                                <label class="control-label" for="name">Date fin</label>
                                <input type="date" class="form-control" name="datefin" @if(isset($theme)) value="{{$theme->date_end}}" @endif required>
                            </div>
                            <div class="form-group  col-md-12 ">                                    
                                <label class="control-label" for="name">Sujet</label>
                                <textarea class="form-control" name="subject" required>@if(isset($theme)){{$theme->subject}}@endif</textarea>
                            </div>
                           
                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary save">Save</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    
@endsection

@section('javascript')

@endsection