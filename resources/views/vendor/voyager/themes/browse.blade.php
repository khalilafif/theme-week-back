@extends('voyager::master')
@section('head')
   
@endsection
@section('page_header')
    <h1 class="page-title">
         Themes
        
            <a href="{{ route('create-theme') }}" class="btn btn-success">
                <i class="voyager-plus"></i> {{ __('voyager::generic.add_new') }}
            </a>
        
    </h1>
@stop

@section('content')

    <div class="page-content container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        @if($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                        @endif
                        <table id="dataTable" class="table table-hover">
                            <thead>
                            <tr>   
                                <th>Titre</th>                             
                                <th>du date début</th>
                                <th>au date fin</th>   
                                <th>Posts</th>                             
                                <th class="actions text-right">{{ __('voyager::generic.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($themes as $theme)

                                    <tr>   
                                        <td>                                       
                                            {{$theme->title}}
                                        </td>                                 
                                        <td>                                       
                                            {{$theme->date_begin}}
                                        </td>
                                        <td>       
                                            {{$theme->date_end}}                            
                                        </td>
                                        <td>       
                                            <a href="{{route('list-posts',$theme->id)}}">posts({{count($theme->posts)}})</a>
                                        </td>
                                        <td class="no-sort no-click bread-actions">
                                            
                                            
                                            <button type="button" class="btn btn-sm btn-danger pull-right delete-theme-btn" data-toggle="modal" data-target="#exampleModal" data-id="{{$theme->id}}">
                                                <i class="voyager-trash"></i> {{ __('voyager::generic.delete') }}
                                            </button>
                                            <a href="{{route('edit-theme',$theme->id)}}" class="btn btn-sm btn-primary pull-right edit-reco" style="margin-right:5px;">
                                                <i class="voyager-edit"></i> {{ __('voyager::generic.edit') }}
                                            </a>                              
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal modal-danger fade" tabindex="-1" role="dialog" id="exampleModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} theme?
                    </h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                       
                        <a href="#" class="btn btn-danger pull-right delete-theme">Yes, Delete This </a>
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    
    <!-- DataTables -->
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable({
                "order": [],
                "language": {!! json_encode(__('voyager::datatable'), true) !!},
                "columnDefs": [{"targets": -1, "searchable":  false, "orderable": false}]
                @if(config('dashboard.data_tables.responsive')), responsive: true @endif
            });
        });
        var url="{{URL::to('/')}}"+"/admin/themes/delete/"

        $('.delete-theme-btn').click(function (e) {
            var id=$(this).data('id');
            console.log('id',id);
            $('.delete-theme').attr('href',url+id);

            $('#delete_modal').modal('show');
        });
    </script>
@stop