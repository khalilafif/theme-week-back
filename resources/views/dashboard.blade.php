@extends('voyager::master')
@section('head')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
    <style>
        #example_filter, #example_paginate,#example_info{
            display:none;
        }
    </style>
@endsection
@section('page_header')
    <h1 class="page-title">
         Dashboard
    </h1>
@stop

@section('content')

    <div class="page-content container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        @if($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                        @endif
                        <table id="example" class="display nowrap" style="display:none;">
                        <thead>
                            <tr>
                                <th>Post id</th>
                                <th>Retailer</th>                  
                                <th>Théme</th>
                                <th>Etat</th>
                                <th>Titre</th>   
                                <th>Likes</th>   
                                <th>Comments</th>   
                                <th>Date</th> 
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($posts as $post)
                            <tr> 
                                <td>{{$post->id}}</td>   
                                <td>{{$clients[$post->client_id]->first()->firstname}}
                                    {{$clients[$post->client_id]->first()->lastname}}</td>                               
                                <td>{{$themes[$post->theme_id]}}</td>
                                <td>{{$post->state}}</td>
                                <td>{{$post->title}}</td>
                                <td>{{(isset($likes[$post->id]))? $likes[$post->id] : 0}}</td>
                                <td>{{(isset($comments[$post->id]))? $comments[$post->id] : 0}}
                                        </td>
                                <td>{{$post->created_at}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                        </table>
                        <table id="dataTable" class="table table-hover">
                            <thead>
                            <tr>   
                                <th>Post numéro</th>  
                                <th>Retailer</th>                                                           
                                <th>Théme</th>
                                <th>Etat</th>
                                <th>Titre</th>   
                                <th>Likes</th>  
                                <th>Comments</th>  
                                <th>Date</th>                           
                                <th class="actions text-right">{{ __('voyager::generic.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($posts as $post)

                                    <tr>   
                                        <td>{{$post->id}}</td>  
                                        <td><a href="{{route('client-info',$post->client_id)}}">{{$clients[$post->client_id]->first()->firstname}}
                                    {{$clients[$post->client_id]->first()->lastname}}</a></td>                               
                                        <td>                                       
                                            <a href="{{route('edit-theme',$post->theme_id)}}">{{$themes[$post->theme_id]}}</a>
                                        </td>
                                        <td>{{$post->state}} </td>
                                        <td>{{$post->title}} </td>
                                        <td>       
                                            <a href="{{route('list-like',$post->id)}}">{{(isset($likes[$post->id]))? $likes[$post->id] : 0}}</a>
                                        </td>
                                        <td><a href="{{route('list-comment',$post->id)}}">{{(isset($comments[$post->id]))? $comments[$post->id] : 0}}</a>
                                        </td>
                                        <td>{{$post->created_at}}</td>
                                        <td class="no-sort no-click bread-actions">
                                            <a href="{{route('edit-post',$post->id)}}" class="btn btn-sm btn-primary pull-right edit-reco" >
                                                <i class="voyager-edit"></i> {{ __('voyager::generic.edit') }}
                                            </a>                              
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    
    <!-- DataTables -->
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable({
                "order": [],
                "language": {!! json_encode(__('voyager::datatable'), true) !!},
                "columnDefs": [{"targets": -1, "searchable":  false, "orderable": false}]
                @if(config('dashboard.data_tables.responsive')), responsive: true @endif
            });
        });        
    </script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
    <script>
    $(document).ready(function() {
        $('#example').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                {extend: 'excel',
                text: 'Export excel',
                className: 'btn btn-primary'}
            ]
        } );
    } );
    </script>
@stop