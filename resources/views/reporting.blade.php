@extends('voyager::master')
@section('head')
    
    
    <link rel="stylesheet" href="{{asset('reporting/style.css?v=14')}}">
@endsection
@section('page_header')
    <h1 class="page-title">
         Reporting
    </h1>
@stop

@section('content')

    <div class="page-content container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-3">
                <div class="panel panel-bordered" id="change-date-bloc">
                    <div class="panel-body">
                        @if($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                        @endif 
                        
                            <form method="POST" action="{{route('change-date')}}" id="form-change-date">
                                <!-- CSRF TOKEN -->
                                @csrf
                            <div class="form-group">
                              <label for="global-theme">Thème </label>                             
                              <select name="theme" class="form-control" id="global-theme">
                                <option value="0">Choisir un thème </option>
                                @foreach($themes as $theme)
                                  <option value="{{$theme->id}}" @if(!$global_stat && $sTheme && $theme->id==$sTheme->id)selected @endif>{{$theme->title}}</option>
                                @endforeach
                              </select> 
                            </div>
                            <div class="form-group">
                              <label for="inp-debut">Date Début </label>
                              <input class="form-control datetime date_debut" name="datedebut" type="date" value="{{$date_debut}}" id="inp-debut">
                            </div>

                            <div class="form-group">
                              <label for="inp-fin">Date Fin </label>
                              <input class="form-control datetime date_fin" name="datefin" type="date" value="{{$date_fin}}" id="inp-fin">
                            </div>                  
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary pull-right">Sauvegarder</button>
                            </div>
                            </form>
                        
                    </div>
                </div>
              </div>
               <div class="col-md-4"> 
                  <div class="panel panel-bordered">
                    <div class="panel-body">
                      <h4>Nombre de poste par théme</h4>
                      <p class="desc-chart">Nombre de posts par thème  pendant la période sélectionnée</p>
                      <div id="pie-nb-post-by-theme"></div>
                    </div>
                  </div>
                </div>
              <div class="col-md-5">
                <div class="panel panel-bordered" id="global-chart-bloc">
                  <div class="panel-body">
                    <h4>Global</h4> 
                      <!--<div class="filtre-bloc ">
                        <ul>
                          <li class="filter" data-type="Y-m-d H:00" data-id="11">H</li>
                          <li class="filter active" data-type="Y-m-d" data-id="11">D</li>
                          <li class="filter" data-type="W-F" data-id="11">W</li>
                          <li class="filter" data-type="M" data-id="11">M</li>
                        </ul>
                      </div>-->
                     <div id="global-chart"></div>
                  </div>
                </div>
              </div>
              </div>
              <div class="row"> 
                <div class="col-md-6"> 
                  <div class="panel panel-bordered">
                    <div class="panel-body">
                      <!--$countUnicCilent['total'] -->
                      <h4>Utilisateurs uniques <span class="pull-right">{{$countUnicCilent['total']}}</span></h4>
                      <p class="desc-chart">Utilisateurs qui ont initié au moins une session pendant la période sélectionnée</p>
                      <div class="filtre-bloc ">
                        <ul>
                          <li class="filter" data-type="Y-m-d H:00" data-id="1">H</li>
                          <li class="filter active" data-type="Y-m-d" data-id="1">D</li>
                          <li class="filter" data-type="W-F" data-id="1">W</li>
                          <li class="filter" data-type="M" data-id="1">M</li>
                        </ul>
                      </div>
                      <div id="unic-retailer-chart"></div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6"> 
                  <div class="panel panel-bordered">
                    <div class="panel-body">
                      <h4>Utilisateurs actifs <span class="pull-right">{{$countActiveClientByDate['total']}}</span></h4>
                      <p class="desc-chart">Utilisateurs qui ont fait une action (like ou partage) pendant la période sélectionnée</p>
                      <div class="filtre-bloc ">
                        <ul>
                          <li class="filter" data-type="Y-m-d H:00" data-id="2">H</li>
                          <li class="filter active" data-type="Y-m-d" data-id="2">D</li>
                          <li class="filter" data-type="W-F" data-id="2">W</li>
                          <li class="filter" data-type="M" data-id="2">M</li>
                        </ul>
                      </div>
                      <div id="active-retailer-chart"></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row"> 
                <div class="col-md-6"> 
                  <div class="panel panel-bordered">
                    <div class="panel-body">
                      <h4>Nouveaux Utilisateurs <span class="pull-right">{{$countNewCilent['total']}}</span></h4>
                      <p class="desc-chart">Utilisateurs venant pour la première fois au cours de la période sélectionnée.</p>
                      <div class="filtre-bloc ">
                        <ul>
                          <li class="filter" data-type="Y-m-d H:00" data-id="3">H</li>
                          <li class="filter active" data-type="Y-m-d" data-id="3">D</li>
                          <li class="filter" data-type="W-F" data-id="3">W</li>
                          <li class="filter" data-type="M" data-id="3">M</li>
                        </ul>
                      </div>
                      <div id="new-retailer-chart"></div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6"> 
                  <div class="panel panel-bordered">
                    <div class="panel-body">
                      <h4>Nombre de sessions <span class="pull-right">{{$chartSessionByDate['total']}}</span></h4>
                      <p class="desc-chart">Nombre total de sessions sur la période sélectionnée</p>
                      <div class="filtre-bloc ">
                        <ul>
                          <li class="filter" data-type="Y-m-d H:00" data-id="4">H</li>
                          <li class="filter active" data-type="Y-m-d" data-id="4">D</li>
                          <li class="filter" data-type="W-F" data-id="4">W</li>
                          <li class="filter" data-type="M" data-id="4">M</li>
                        </ul>
                      </div>
                      <div id="session-duration-date"></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row"> 
                <div class="col-md-8"> 
                  <div class="panel panel-bordered">
                    <div class="panel-body">
                      <!-- {{gmdate("H:i:s",$chartSessionByDate['total']/60 )}}-->
                      <h4>Durée moyenne des sessions <span class="pull-right">{{$avgSessionDuration}} Min</span></h4>
                      <p class="desc-chart">Il s'agit de la durée moyenne d'une session  pendant la période sélectionnée</p>

                      <div class="filtre-bloc ">
                        <ul>
                          <li class="filter" data-type="Y-m-d H:00" data-id="5">H</li>
                          <li class="filter active" data-type="Y-m-d" data-id="5">D</li>
                          <li class="filter" data-type="W-F" data-id="5">W</li>
                          <li class="filter" data-type="M" data-id="5">M</li>
                        </ul>
                      </div>
                      <div id="session-duration-chart"></div>
                    </div>
                  </div>
                </div>                
                <div class="col-md-4"> 
                  <div class="panel panel-bordered">
                    <div class="panel-body">
                      <h4>Utilisateurs par region</h4>
                      <p class="desc-chart">Nombre d'utilisateur dans chaque région pendant la période sélectionnée</p>
                      <div id="pie-region"></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row"> 
                <div class="col-md-4"> 
                  <div class="panel panel-bordered">
                    <div class="panel-body">
                      <h4>Total posts <span class="pull-right">{{$countPostByDate['total']}}</span></h4>
                      <p class="desc-chart">Nombre total des posts partagés pendant la période sélectionnée</p>
                      <div class="filtre-bloc ">
                        <ul>
                          <li class="filter" data-type="Y-m-d H:00" data-id="6">H</li>
                          <li class="filter active" data-type="Y-m-d" data-id="6">D</li>
                          <li class="filter" data-type="W-F" data-id="6">W</li>
                          <li class="filter" data-type="M" data-id="6">M</li>
                        </ul>
                      </div>
                      <div id="posts-chart"></div>
                    </div>
                  </div>
                </div>
                <div class="col-md-4"> 
                  <div class="panel panel-bordered">
                    <div class="panel-body">
                      <h4>Total Likes <span class="pull-right">{{$countLikeByDate['total']}}</span></h4>
                      <p class="desc-chart">Nombre total des likes pendant la période sélectionnée</p>
                      <div class="filtre-bloc ">
                        <ul>
                          <li class="filter" data-type="Y-m-d H:00" data-id="7">H</li>
                          <li class="filter active" data-type="Y-m-d" data-id="7">D</li>
                          <li class="filter" data-type="W-F" data-id="7">W</li>
                          <li class="filter" data-type="M" data-id="7">M</li>
                        </ul>
                      </div>
                      <div id="likes-chart"></div>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 top-post-bloc"> 
                  <div class="panel panel-bordered">
                    <div class="panel-body">
                      <h4>Le post le plus liké</h4>
                      <!--<select class=" theme-top-post">
                          @foreach($themes as $theme)
                            <option value="{{$theme->id}}" @if($sTheme && $theme->id==$sTheme->id)selected @endif>{{$theme->title}}</option>
                          @endforeach
                      </select>--><br> 
                      <h4 style="text-align: right;">@if(!is_null($topPost)){{$topPost->theme->title}}@else Ce thème ne contient pas de posts pendant cette période @endif</h4>
                      <p id="post-title">@if(!is_null($topPost)){{$topPost->title}} <span class="nb-likes">Likes({{$topPost->likes_count}})</span>@endif</p>
                      <p id="post-description">@if(!is_null($topPost)){{$topPost->description}}@endif</p>
                      @if(!is_null($topPost))
                      <img id="post-media"  @if(is_null($topPost->media)) class="hidden" @endif src="{{$topPost->media}}" >
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="panel panel-bordered">
                    <div class="panel-body">
                      <h4>Total comments <span class="pull-right">{{$countCommentByDate['total']}}</span></h4>
                      <p class="desc-chart">Nombre total des commentaires pendant la période sélectionnée</p>
                      <div class="filtre-bloc ">
                        <ul>
                          <li class="filter" data-type="Y-m-d H:00" data-id="8">H</li>
                          <li class="filter active" data-type="Y-m-d" data-id="8">D</li>
                          <li class="filter" data-type="W-F" data-id="8">W</li>
                          <li class="filter" data-type="M" data-id="8">M</li>
                        </ul>
                      </div>
                      <div id="comments-chart"></div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="panel panel-bordered">
                    <div class="panel-body">
                      <h4>Total comments Likes <span class="pull-right">{{$countLikecByDate['total']}}</span></h4>
                      <p class="desc-chart">Nombre total de likes des commentaires pendant la période sélectionnée</p>
                      <div class="filtre-bloc ">
                        <ul>
                          <li class="filter" data-type="Y-m-d H:00" data-id="9">H</li>
                          <li class="filter active" data-type="Y-m-d" data-id="9">D</li>
                          <li class="filter" data-type="W-F" data-id="9">W</li>
                          <li class="filter" data-type="M" data-id="9">M</li>
                        </ul>
                      </div>
                      <div id="like-comments-chart"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
@stop
                        
@section('javascript')
  <!--<script src="https://code.highcharts.com/highcharts.js"></script>  -->
  <script src="https://code.highcharts.com/stock/highstock.js"></script>
  <script src="https://code.highcharts.com/modules/series-label.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <script src="https://code.highcharts.com/modules/export-data.js"></script>
  <script src="https://code.highcharts.com/modules/accessibility.js"></script>
  <script src="https://code.highcharts.com/modules/variable-pie.js"></script>
    <script>
      var token= "{!! csrf_token() !!}";
      var date_debut="{!!$date_debut!!}";
      var date_fin="{!!$date_fin!!}";
      var period={!!json_encode(array_keys($period))!!};
      var regions={!!json_encode($regions)!!};
      var themes_name={!!json_encode($nbPostByTheme['themes'])!!}
      var nbPostByTheme={!!json_encode($nbPostByTheme['series'])!!};
      var unicRetailers={!!json_encode($countUnicCilent['chart'])!!};
      console.log('countUnicCilent',unicRetailers);
      var newRetailers={!!json_encode($countNewCilent['chart'])!!};
      var activeRetailers={!!json_encode($countActiveClientByDate['chart'])!!};
      var comments={!!json_encode($countCommentByDate['chart'])!!};
      var likecs={!!json_encode($countLikecByDate['chart'])!!};
      var posts={!!json_encode($countPostByDate['chart'])!!};
      var likes={!!json_encode($countLikeByDate['chart'])!!};
      var durations={!!json_encode($countDurationByDate['chart'])!!};
      console.log('durations',durations);
      var sessionDurationsDate={!!json_encode($chartSessionByDate['chart'])!!};
      var theme_title={!!json_encode($themes->pluck('title','id')->all())!!};
      var theme_begins={!!json_encode($themes->pluck('date_begin','id')->all())!!};
      var theme_ends={!!json_encode($themes->pluck('date_end','id')->all())!!};
      var routeTopPost="{{route('get-top-post',0)}}";
      var routeSharePost="{{route('shared-post',0)}}";
      var routeFilterChart="{{route('filter-chart')}}";
      var heightChangeDate=0;       
    </script>
   
    <script type="text/javascript" src="{{asset('reporting/reporting.js?v=16')}}"></script>
    
@stop
